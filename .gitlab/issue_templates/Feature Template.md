## Summary

(Summarize the feature request concisely)

## Feature Description

(A more detailed description of the feature including how you expect it to behave)

## Justification

(Why this feature is needed - it's important you make a valid case for the feature. 
Does it address an existing problem? Does it provide a tangible benefit to end users? Who is this useful for?)

## Example Usage/Code

(How this feature might be used)

## Alternatives

(What alternatives have you considered?)

