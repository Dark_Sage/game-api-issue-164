package com.runemate.game.cache.file;

public class InitialChunkInfo {
    private final int size;
    private final int index;
    private final int group;

    public InitialChunkInfo(int group, int size, int index) {
        this.group = group;
        this.size = size;
        this.index = index;
    }

    public int getGroup() {
        return group;
    }

    public int getSize() {
        return size;
    }

    public int getIndex() {
        return index;
    }
}
