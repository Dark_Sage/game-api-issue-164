package com.runemate.game.cache.util;

import com.runemate.game.cache.io.*;
import java.io.*;
import java.util.*;
import java.util.zip.*;

public final class Bytes {
    private Bytes() {
    }

    public static byte[] decompress(byte[] data, int[] xteaKeys) throws IOException {
        Js5InputStream stream = new Js5InputStream(data);
        int type = stream.readUnsignedByte();
        int size = stream.readInt();
        if (size < 0) {
            throw new StreamCorruptedException(
                "Unable to decompress the provided data because it's supposedly " + size
                    + " bytes long.");
        }
        boolean providedXteaKeys = false;
        if (xteaKeys != null && xteaKeys.length == 4 && (
            xteaKeys[0] != 0 || xteaKeys[1] != 0
                || xteaKeys[2] != 0 || xteaKeys[3] != 0
        )) {
            providedXteaKeys = true;
            stream.decipherXTEA(xteaKeys, size + (type == 0 ? 0 : 4));
        }
        if (type == 0) {
            byte[] bytes = new byte[size];
            int actual = stream.read(bytes);
            if (actual != size) {
                throw new StreamCorruptedException("Only " + actual + " out of the expected " + size
                    + " bytes were able to be read (xtea keys=" + Arrays.toString(xteaKeys) + ')');
            }
            return bytes;
        } else {
            int newSize = stream.readInt();
            if (newSize < 0) {
                if (providedXteaKeys) {
                    throw new OutdatedXTEAKeyException(xteaKeys);
                } else if (size != 21) {
                    throw new MissingXTEAKeyException();
                } else {
                    throw new EmptyLandscapeException();
                }
            }
            byte[] bytes = new byte[newSize];
            byte[] payload = stream.getBackingArray();
            if (type == 1) {
                try {
                    BZip2Decompressor.decompress(bytes, bytes.length, payload, 9);
                } catch (ArrayIndexOutOfBoundsException aioobe) {
                    throw new StreamCorruptedException("Unable to decompress the cache entry ");
                }
            } else if (type == 2) {
                Inflater inflater = new Inflater(true);
                inflater.setInput(payload, stream.position() + 10,
                    stream.size() - (stream.position() + 18)
                );
                try {
                    inflater.inflate(bytes);
                } catch (DataFormatException dfe) {
                    if (providedXteaKeys) {
                        throw new OutdatedXTEAKeyException(xteaKeys);
                    } else if (payload.length != 32) {
                        throw new MissingXTEAKeyException();
                    } else {
                        throw new EmptyLandscapeException();
                    }
                }
                inflater.reset();
            } else {
                throw new StreamCorruptedException(
                    "The compression type with opcode " + type + " is not currently supported.");
            }
            return bytes;
        }
    }

    public static int join(byte[] bytes, int offset) {
        int builder = 0;
        for (int i = 0; i < 4 && i + offset < bytes.length; i++) {
            builder <<= 8;
            builder |= (bytes[i] & 0xFF);
        }
        return builder;
    }
}
