package com.runemate.game.cache.util;

public class OutdatedXTEAKeyException extends RuntimeException {
    private final int[] keys;

    public OutdatedXTEAKeyException(int[] keys) {
        this.keys = keys;
    }

    public int[] getKeys() {
        return keys;
    }
}
