package com.runemate.game.api.script.framework.listeners.events;

public interface EntityEvent extends Event {

    EntityType getEntityType();

    enum EntityType {
        NPC, PLAYER, GAMEOBJECT, SPOTANIMATION, GROUNDITEM, PROJECTILE
    }

    static EntityType resolveType(String type) {
        switch (type) {
            case "Npc":
                return EntityType.NPC;
            case "Player":
                return EntityType.PLAYER;
            case "EventObject":
            case "WallObject":
            case "BoundaryObject":
            case "FloorObject":
                return EntityType.GAMEOBJECT;
            case "SpotAnimationEntity":
                return EntityType.SPOTANIMATION;
            case "Projectile":
                return EntityType.PROJECTILE;
            case "ItemPile":
                return EntityType.GROUNDITEM;
            default:
                return null;
        }
    }

}
