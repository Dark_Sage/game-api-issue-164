package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.*;
import lombok.*;

@Value
public class PlayerMovementEvent implements EntityEvent {

    Player player;

    @Override
    public EntityType getEntityType() {
        return EntityType.PLAYER;
    }
}
