package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public interface InventoryListener extends EventListener {
    default void onItemAdded(final ItemEvent event) {
    }

    default void onItemRemoved(final ItemEvent event) {
    }
}
