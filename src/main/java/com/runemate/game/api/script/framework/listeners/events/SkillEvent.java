package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.local.*;
import lombok.*;

@Value
public class SkillEvent implements Event {
    Skill skill;
    Type type;
    int current;
    int previous;
    @Getter(lazy = true)
    int change = calculateChange();

    private int calculateChange() {
        //Boosted levels go down over time
        if (type == Type.CURRENT_LEVEL_CHANGED) {
            return current - previous;
        }
        return Math.max(0, current - previous);
    }

    public enum Type {
        LEVEL_GAINED,
        EXPERIENCE_GAINED,
        CURRENT_LEVEL_CHANGED
    }
}
