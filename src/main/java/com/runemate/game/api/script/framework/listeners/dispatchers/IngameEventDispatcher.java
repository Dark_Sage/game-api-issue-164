package com.runemate.game.api.script.framework.listeners.dispatchers;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.core.*;

public abstract class IngameEventDispatcher implements Runnable {
    public abstract int getIterationRateInMilliseconds();

    public abstract void dispatch(EventDispatcher dispatcher);

    @Override
    public void run() {
        try {
            AbstractBot script = Environment.getBot();
            if (script != null) {
                dispatch(script.getEventDispatcher());
            } else {
                clear();
            }
        } catch (RuntimeException re) {
            re.printStackTrace();
            throw re;
        }
    }

    public void clear() {
    }
}
