package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import lombok.*;

@Value
public class ItemEvent implements Event {
    SpriteItem item;
    int quantityChange;
    @Getter(lazy = true)
    Type type = resolveType();

    private Type resolveType() {
        if (quantityChange > 0) {
            return Type.ADDITION;
        }
        if (quantityChange < 0) {
            return Type.REMOVAL;
        }
        return Type.UNKNOWN;
    }

    public int getQuantityChange() {
        return Math.abs(quantityChange);
    }

    public enum Type {
        ADDITION,
        REMOVAL,
        UNKNOWN
    }
}
