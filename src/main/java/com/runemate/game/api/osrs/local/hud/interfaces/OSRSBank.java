package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.exceptions.*;
import java.awt.event.*;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;

public final class OSRSBank {
    public static final int DEFAULT_QUANTITY_VARBIT = 6590;
    public static final int CUSTOM_DEFAULT_QUANTITY_VARBIT = 3960;
    private static final Pattern BANK_OPEN_TITLE_PATTERN =
        Pattern.compile("The Bank of Gielinor|Tab [1-9]|Bank settings menu|Showing items: .+");
    private static final int BANK_CONTAINER = 12;

    private OSRSBank() {
    }

    public static boolean isOpen() {
        return !Interfaces.newQuery().containers(BANK_CONTAINER).grandchildren(false)
            .types(InterfaceComponent.Type.LABEL)
            .texts(BANK_OPEN_TITLE_PATTERN)
            .visible().results().isEmpty();
    }

    public static boolean close(boolean hotkey) {
        if (!Bank.isOpen()) {
            return false;
        }
        if (hotkey && OptionsTab.AllSettings.INTERFACES_CLOSABLE_WITH_ESCAPE.isEnabled()) {
            if (Keyboard.typeKey(KeyEvent.VK_ESCAPE)) {
                return Execution.delayWhile(Bank::isOpen, 2000, 2500);
            }
        } else {
            InterfaceComponent button =
                Interfaces.newQuery().containers(BANK_CONTAINER).grandchildren(true)
                    .types(InterfaceComponent.Type.SPRITE).actions("Close").visible().results()
                    .first();
            if (button != null && button.interact("Close")) {
                return Execution.delayWhile(Bank::isOpen, 2000);
            }
        }
        return false;
    }

    public static boolean deposit(final SpriteItem item, final int amount) {
        if (getCurrentTab() > 0 && !openFirstTab()) {
            return false;
        }
        if (item == null || !item.hover()) {
            return false;
        }
        boolean manual = false;
        String action = "Deposit";
        Set<String> actions =
            Menu.getItems().stream().map(MenuItem::getAction).collect(Collectors.toSet());
        if (Inventory.getQuantity(item.getId()) == 1 && actions.contains(action + "-1")) {
            action += "-1";
        } else if ((amount == 0 || amount >= item.getQuantity()) &&
            actions.contains(action + "-All")) {
            action += "-All";
        } else if (actions.contains(action + '-' + amount)) {
            action += "-" + amount;
        } else if (amount != 0 && actions.contains(action + "-X")) {
            manual = true;
            action += "-X";
        } else if (amount == 0 && actions.contains(action + " All")) {//DepositBoxes
            action += " All";
        } else if (actions.contains(action + ' ' + amount)) {//DepositBoxes
            action += " " + amount;
        } else if (amount != 0 && actions.contains(action + " X")) {
            manual = true;
            action += " X";
        } else {
            Environment.getLogger().warn("[Bank] Action not found in " + actions);
        }
        Environment.getLogger().fine("[Bank] Depositing with action: " + action);
        if (actions.contains(action) && item.interact(action)) {
            if (manual && Execution.delayUntil(InputDialog::isOpen, 1400, 2200)) {
                return InputDialog.enterAmount(amount) &&
                    Execution.delayWhile(item::isValid, 1400, 2200);
            }
            return Execution.delayWhile(item::isValid, 1400, 2200);
        }
        return false;
    }

    @SuppressWarnings("Duplicates")
    public static boolean withdraw(final SpriteItem item, final int quantityToWithdraw) {
        if (getCurrentTab() > 0 && !openFirstTab()) {
            return false;
        }
        if (item != null && item.hover()) {
            boolean manual = false;
            String action = "Withdraw";
            int availableQuantity = Bank.getQuantity(item.getId());
            Set<String> actions =
                Menu.getItems().stream().map(MenuItem::getAction).collect(Collectors.toSet());
            if (Objects.equals(Bank.getDefaultQuantity(), Bank.DefaultQuantity.ALL) &&
                quantityToWithdraw == 0) {
                action += "-" + Bank.DefaultQuantity.ALL.getText();
            } else {
                int defaultQuantity = Bank.getExactDefaultQuantity();
                if (quantityToWithdraw == defaultQuantity) {
                    action += "-" + defaultQuantity;
                } else {
                    ItemDefinition def = item.getDefinition();
                    final boolean willFillInventory = def != null && !def.stacks() &&
                        !Objects.equals(Bank.getWithdrawMode(), Bank.WithdrawMode.NOTE)
                        && (Inventory.getEmptySlots() - quantityToWithdraw) <= 0;
                    if ((
                        quantityToWithdraw == 0 || willFillInventory ||
                            availableQuantity <= quantityToWithdraw
                    ) &&
                        actions.contains(action + "-All")) {
                        action += "-All";
                    } else if (quantityToWithdraw == -1 &&
                        actions.contains(action + "-All-but-1")) {
                        action += "-All-but-1";
                    } else if (actions.contains(action + '-' + quantityToWithdraw)) {
                        action += "-" + quantityToWithdraw;
                    } else if (quantityToWithdraw != 0 && actions.contains(action + "-X")) {
                        manual = true;
                        action += "-X";
                    } else {
                        Environment.getLogger().warn("[Bank] Action not found in " + actions);
                    }
                }
            }
            Environment.getLogger().fine("[Bank] Withdrawing with action: " + action);
            if (actions.contains(action) && item.interact(action)) {
                if (manual && Execution.delayUntil(InputDialog::isOpen, 1200, 2400)) {
                    return InputDialog.enterAmount(quantityToWithdraw) &&
                        Execution.delayWhile(item::isValid, 1200, 2400);
                }
                return Execution.delayWhile(item::isValid, 1200, 2400);
            }
        }
        return false;
    }

    private static InterfaceComponent getSearchDialogComponent() {
        return Interfaces.newQuery().containers(162).types(InterfaceComponent.Type.LABEL)
            .grandchildren(false).texts("Show items whose names contain the following text:")
            .results().first();
    }

    private static InterfaceComponent getSearchTextComponent() {
        return Interfaces.newQuery().containers(162).types(InterfaceComponent.Type.LABEL)
            .grandchildren(false).heights(20).fonts(496).textContains("*").results().first();
    }

    public static boolean withdrawToBeastOfBurden(final SpriteItem item, final int amount) {
        if (Environment.getBot().getMetaData().isOSRSOnly()) {
            throw new OSRSException("Bank", "withdrawToBeastOfBurden");
        }
        return false;
    }

    public static boolean depositInventory() {
        if (!Inventory.getItems().isEmpty()) {
            final InterfaceComponent button = Interfaces.newQuery().containers(BANK_CONTAINER)
                .types(InterfaceComponent.Type.SPRITE).actions("Deposit inventory")
                .grandchildren(false).results().first();
            if (button != null && button.isVisible() && button.click()) {
                Execution.delayUntil(Inventory::isEmpty, 2000);
            }
        }
        return Inventory.getItems().isEmpty();
    }

    public static boolean depositWornItems() {
        if (!Equipment.getItems().isEmpty()) {
            final InterfaceComponent button = Interfaces.newQuery().containers(BANK_CONTAINER)
                .types(InterfaceComponent.Type.SPRITE).actions("Deposit worn items")
                .grandchildren(false).results().first();
            if (button != null && button.isVisible() && button.click()) {
                Execution.delayUntil(Equipment::isEmpty, 2000);
            }
        }
        return Equipment.isEmpty();
    }

    public static InteractableRectangle getViewport() {
        InterfaceComponent viewport =
            Interfaces.newQuery().grandchildren(false).containers(BANK_CONTAINER)
                .types(InterfaceComponent.Type.CONTAINER)
                .widths(460).visible().results().first();
        return viewport != null ? viewport.getBounds() : null;
    }

    public static List<InteractableRectangle> getSlotBounds() {
        final InterfaceComponent container =
            Interfaces.newQuery().containers(BANK_CONTAINER)
                .grandchildren(false)
                .types(InterfaceComponent.Type.CONTAINER)
                .filter(it -> it.getChildQuantity() >= 800)
                .results().first();
        return container != null
            ?
            Parallelize.mapAndCollectToList(container.getChildren(), InterfaceComponent::getBounds)
            : Collections.emptyList();
    }

    public static InteractableRectangle getBoundsOf(int index) {
        InterfaceComponent component = Interfaces.newQuery()
            .containers(BANK_CONTAINER)
            .grandchildren(false)
            .types(InterfaceComponent.Type.CONTAINER)
            .filter(it -> it.getChildQuantity() >= 800)
            .results().first();
        if (component != null) {
            final InterfaceComponent c = component.getChild(index);
            if (c != null) {
                return c.getBounds();
            }
        }
        return null;
    }

    public static int getCurrentTab() {
        Varbit varbit = Varbits.load(4150);
        return varbit != null ? varbit.getValue() : -1;
    }

    public static boolean openFirstTab() {
        if (getCurrentTab() == 0) {
            return true;
        }
        final InterfaceComponent first = getFirstTabComponent();
        return first != null && first.isVisible() && first.interact("View all items");
    }

    private static InterfaceComponent getFirstTabComponent() {
        return Interfaces.newQuery().containers(BANK_CONTAINER)
            .types(InterfaceComponent.Type.SPRITE).actions("View all items").results().first();
    }
}
