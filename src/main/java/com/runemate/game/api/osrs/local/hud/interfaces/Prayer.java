package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;

public enum Prayer {
    THICK_SKIN(115, 0),
    BURST_OF_STRENGTH(116, 1),
    CLARITY_OF_THOUGHT(117, 2),
    ROCK_SKIN(118, 3),
    SUPERHUMAN_STRENGTH(119, 4),
    IMPROVED_REFLEXES(120, 5),
    RAPID_RESTORE(121, 6),
    RAPID_HEAL(122, 7),
    PROTECT_ITEM(123, 8),
    STEEL_SKIN(124, 9),
    ULTIMATE_STRENGTH(125, 10),
    INCREDIBLE_REFLEXES(126, 11),
    PROTECT_FROM_MAGIC(127, 12),
    PROTECT_FROM_MISSILES(128, 13),
    PROTECT_FROM_MELEE(129, 14),
    RETRIBUTION(131, 15),
    REDEMPTION(130, 16),
    SMITE(132, 17),
    SHARP_EYE(133, 18),
    MYSTIC_WILL(134, 19),
    HAWK_EYE(502, 20),
    MYSTIC_LORE(503, 21),
    EAGLE_EYE(504, 22),
    MYSTIC_MIGHT(505, 23),
    CHIVALRY(945, 25),
    PIETY(946, 26),
    RIGOUR(1420, 24),
    AUGURY(1421, 27),
    PRESERVE(947, 28);
    private static final int ACTIVATED_PRAYERS = 83;
    private static final int PRAYER_INTERFACE = 541;
    private static final int QUICK_PRAYERS_ACTIVATED = 375;
    private static final int QUICK_PRAYER_BUTTON_CONTAINER = 160;
    private static final int SELECTED_QUICK_PRAYERS = 84;
    private static final int LEVEL_REQUIREMENT_ENUM_DEFINITION = 861;
    private static final Pattern TOGGLE_ACTION = Pattern.compile("^(Dea|A)ctivate$");
    private final String name;
    private final int spriteId;
    private final int varpIndex;

    Prayer(int spriteId, int varpIndex) {
        String[] words = name().split("_");
        StringBuilder name = new StringBuilder();
        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            if (i != 0) {
                name.append(' ');
            }
            name.append("OF".equals(word) || "FROM".equals(word) ? word.toLowerCase() :
                word.charAt(0) + word.substring(1).toLowerCase());
        }
        this.name = name.toString();
        this.spriteId = spriteId;
        this.varpIndex = varpIndex;
    }

    public static List<Prayer> getActivePrayers() {
        List<Prayer> active = new ArrayList<>();
        Varp activated = Varps.getAt(ACTIVATED_PRAYERS);
        for (Prayer p : values()) {
            if (activated.getValueOfBit(p.varpIndex) == 1) {
                active.add(p);
            }
        }
        return active;
    }

    public static List<Prayer> getSelectedQuickPrayers() {
        return Arrays.stream(values()).filter(Prayer::isSelectedAsQuickPrayer)
            .collect(Collectors.toList());
    }

    public static boolean isQuickPraying() {
        return Varps.getAt(QUICK_PRAYERS_ACTIVATED).getValueOfBit(0) == 1;
    }

    public static boolean toggleQuickPrayers() {
        InterfaceComponent toggleComponent = getQuickPrayerToggle();
        if (toggleComponent != null) {
            boolean activated = isQuickPraying();
            if (toggleComponent.interact(TOGGLE_ACTION, "Quick-prayers")) {
                return Execution.delayUntil(() -> isQuickPraying() != activated, 2000);
            }
        }
        return false;
    }

    private static InterfaceComponent getQuickPrayerToggle() {
        return Interfaces.newQuery().containers(QUICK_PRAYER_BUTTON_CONTAINER)
            .types(InterfaceComponent.Type.CONTAINER).actions(TOGGLE_ACTION).results().first();
    }

    private static boolean openQuickPrayerSetup() {
        final InterfaceComponent mmButton = getQuickPrayerToggle();
        return isQuickPrayerSetupOpen() || mmButton != null && mmButton.interact("Setup")
            && Execution.delayUntil(Prayer::isQuickPrayerSetupOpen, 1200, 1800);
    }

    /**
     * Sets the players quick-prayers to be only the ones provided
     *
     * @param prayers prayers to set
     * @return if selected quick-prayers are correct and we successfully confirmed
     */
    public static boolean setQuickPrayers(final Prayer... prayers) {
        return setQuickPrayers(Arrays.asList(prayers));
    }

    public static boolean setQuickPrayers(final Collection<Prayer> prayers) {
        if (openQuickPrayerSetup()) {
            for (final Prayer prayer : values()) {
                if (prayer.isSelectedAsQuickPrayer() != prayers.contains(prayer)
                    && toggleQuickPrayerSelected(prayer)) {
                    break;
                }
            }
        }
        return Arrays.stream(Prayer.values())
            .allMatch(p -> p.isSelectedAsQuickPrayer() == prayers.contains(p))
            && confirmQuickPrayerSelection();
    }

    private static boolean toggleQuickPrayerSelected(final Prayer prayer) {
        final boolean selected = prayer.isSelectedAsQuickPrayer();
        final InterfaceComponent ic =
            Interfaces.newQuery().containers(77).types(InterfaceComponent.Type.SPRITE)
                .names(prayer.toString()).actions("Toggle").results().first();
        return ic != null && ic.isVisible() && ic.interact("Toggle") && Execution.delayUntil(
            () -> prayer.isSelectedAsQuickPrayer() != selected, 1200, 1800);
    }

    /**
     * Determines whether the quick-prayer configuration menu is open or not
     *
     * @return if the quick-prayer config menu is open
     */
    //Exposing in case setQuickPrayers fails to confirm and user needs to confirm the interface has closed
    public static boolean isQuickPrayerSetupOpen() {
        final InterfaceComponent ic = getQuickPrayerConfirmButton();
        return ic != null && ic.isVisible();
    }

    private static InterfaceComponent getQuickPrayerConfirmButton() {
        return Interfaces.newQuery().containers(77).types(InterfaceComponent.Type.SPRITE)
            .actions("Done").results().first();
    }

    public static boolean confirmQuickPrayerSelection() {
        final InterfaceComponent ic =
            Interfaces.newQuery().containers(77).types(InterfaceComponent.Type.SPRITE)
                .actions("Done").results().first();
        return !isQuickPrayerSetupOpen() || ic != null && ic.isVisible() && ic.interact("Done")
            && Execution.delayWhile(Prayer::isQuickPrayerSetupOpen, 1200, 1800);
    }

    public static int getPoints() {
        return Skill.PRAYER.getCurrentLevel();
    }

    public static int getMaximumPoints() {
        return Skill.PRAYER.getBaseLevel();
    }

    public boolean isActivatable() {
        Varbit unidentifiedVarbit = Varbits.load(5314);
        if (unidentifiedVarbit == null) {
            return false;
        }
        int unidentifiedVarbitValue = unidentifiedVarbit.getValue();
        if (Skill.PRAYER.getBaseLevel() < getRequiredLevel() && unidentifiedVarbitValue == 0) {
            return false;
        }
        WorldOverview worldOverview = Worlds.getCurrentOverview();
        if (worldOverview == null) {
            return false;
        }
        Varbit kingsRansomVarbit = Varbits.load(3909);
        switch (varpIndex) {
            case 8:
                if (unidentifiedVarbitValue == 1
                    || worldOverview.getWorldTypes().contains(WorldType.HIGH_RISK)
                    || worldOverview.getWorldTypes().contains(WorldType.DEADMAN)) {
                    return false;
                }
                break;
            case 15:
            case 16:
            case 17:
                if (unidentifiedVarbitValue == 0 && !worldOverview.isMembersOnly()) {
                    return false;
                }
                break;
            case 24:
                Varbit rigourVarbit = Varbits.load(5451);
                if ((
                    unidentifiedVarbitValue == 0 && (
                        rigourVarbit == null
                            || rigourVarbit.getValue() == 0
                    )
                ) || !worldOverview.isMembersOnly()
                    || Skill.DEFENCE.getBaseLevel() < 70) {
                    return false;
                }
                break;
            case 25:
                if ((
                    unidentifiedVarbitValue == 0 && (
                        kingsRansomVarbit == null
                            || kingsRansomVarbit.getValue() < 8
                    )
                ) || !worldOverview.isMembersOnly()
                    || Skill.DEFENCE.getBaseLevel() < 65) {
                    return false;
                }
                break;
            case 26:
                if ((
                    unidentifiedVarbitValue == 0 && (
                        kingsRansomVarbit == null
                            || kingsRansomVarbit.getValue() < 8
                    )
                ) || !worldOverview.isMembersOnly()
                    || Skill.DEFENCE.getBaseLevel() < 70) {
                    return false;
                }
                break;
            case 27:
                Varbit auguryVarbit = Varbits.load(5452);
                if ((
                    unidentifiedVarbitValue == 0 && (
                        auguryVarbit == null
                            || auguryVarbit.getValue() == 0
                    )
                ) || !worldOverview.isMembersOnly()
                    || Skill.DEFENCE.getBaseLevel() < 70) {
                    return false;
                }
                break;
            case 28:
                Varbit preserveVarbit = Varbits.load(5453);
                if (unidentifiedVarbitValue == 1 || preserveVarbit == null
                    || preserveVarbit.getValue() == 0 || !worldOverview.isMembersOnly()) {
                    return false;
                }
                break;
            default:
                return true;
        }
        return true;
    }

    public boolean activate() {
        return isActivated() || toggle();
    }

    public boolean deactivate() {
        return !isActivated() || toggle();
    }

    public InterfaceComponent getComponent() {
        return Interfaces.newQuery().containers(PRAYER_INTERFACE).grandchildren(false)
            .types(InterfaceComponent.Type.CONTAINER).names(name).results().first();
    }

    public int getRequiredLevel() {
        EnumDefinition enumDefinition = EnumDefinitions.load(LEVEL_REQUIREMENT_ENUM_DEFINITION);
        return enumDefinition != null ? enumDefinition.getInt(varpIndex) : -1;
    }

    public boolean isActivated() {
        return Varps.getAt(ACTIVATED_PRAYERS).getValueOfBit(varpIndex) == 1;
    }

    public boolean isSelectedAsQuickPrayer() {
        return Varps.getAt(SELECTED_QUICK_PRAYERS).getValueOfBit(varpIndex) == 1;
    }

    @Override
    public String toString() {
        return name;
    }

    private boolean toggle() {
        final boolean initial = isActivated();
        if (Skill.PRAYER.getBaseLevel() >= getRequiredLevel()
            && Skill.PRAYER.getCurrentLevel() > 0) {
            if (ControlPanelTab.PRAYER.isOpen() || ControlPanelTab.PRAYER.open()) {
                if (isQuickPrayerSetupOpen()) { //Account for Quickprayer setup support
                    confirmQuickPrayerSelection();
                    return false;
                }
                final InterfaceComponent button = getComponent();
                if (button != null && button.isVisible() && button.click()) {
                    return Execution.delayUntil(() -> isActivated() != initial, 2000);
                }
            }
        }
        return isActivated() != initial;
    }
}