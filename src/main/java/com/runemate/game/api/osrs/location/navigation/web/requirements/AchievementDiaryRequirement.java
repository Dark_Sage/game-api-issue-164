package com.runemate.game.api.osrs.location.navigation.web.requirements;

import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.osrs.local.*;
import java.io.*;
import java.util.*;
import lombok.*;

public class AchievementDiaryRequirement extends WebRequirement implements SerializableRequirement {
    private AchievementDiary diary;
    private AchievementDiary.Difficulty difficulty;

    public AchievementDiaryRequirement(int protocol, ObjectInput stream) {
        super(protocol, stream);
    }

    public AchievementDiaryRequirement(
        AchievementDiary diary,
        AchievementDiary.Difficulty difficulty
    ) {
        this.diary = diary;
        this.difficulty = difficulty;
    }

    @Override
    protected boolean isMet0() {
        return diary.isComplete(difficulty);
    }

    @Override
    public int getOpcode() {
        return 12;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(diary.name());
        stream.writeUTF(difficulty.name());
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.diary = AchievementDiary.valueOf(stream.readUTF());
        this.difficulty = AchievementDiary.Difficulty.valueOf(stream.readUTF());
        return true;
    }

    @Override
    public String toString() {
        return "AchievementDiaryRequirement(diary: " + diary + ", difficulty: " + difficulty + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AchievementDiaryRequirement that = (AchievementDiaryRequirement) o;
        return diary == that.diary && difficulty == that.difficulty;
    }

    @Override
    public int hashCode() {
        return Objects.hash(diary, difficulty);
    }
}
