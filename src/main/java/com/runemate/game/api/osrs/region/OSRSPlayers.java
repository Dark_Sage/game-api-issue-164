package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.entities.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public final class OSRSPlayers {

    private OSRSPlayers() {
    }


    public static OSRSPlayer getAt(int index) {
        long playerUid = OpenPlayer.getPlayerByIndex(index);
        if (playerUid != -1L) {
            return new OSRSPlayer(playerUid);
        }
        return null;
    }


    public static LocatableEntityQueryResults<Player> getLoaded(
        final Predicate<? super Player> predicate
    ) {
        final var uids = OpenPlayer.getLoadedClean();
        if (uids != null) {
            return new LocatableEntityQueryResults<>(Arrays.stream(uids)
                .filter(uid -> uid > 0)
                .mapToObj(OSRSPlayer::new)
                .filter(player -> predicate == null || predicate.test(player))
                .collect(Collectors.toList()));
        }
        return new LocatableEntityQueryResults<>(Collections.emptyList());
    }


    public static OSRSPlayer getLocal() {
        long uid = OpenPlayer.getLocal();
        return uid != 0 ? new OSRSPlayer(uid) : null;
    }
}
