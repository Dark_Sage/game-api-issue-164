package com.runemate.game.api.osrs.location.navigation;

import com.runemate.client.framework.open.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;

public final class OSRSTraversal {

    /**
     * @return Run energy as a percentage
     */
    public static int getRunEnergy() {
        return getPreciseRunEnergy() / 100;
    }

    /**
     * @return Run energy read directly from the client, where a value of 10000 is 100%.
     */
    public static int getPreciseRunEnergy() {
        return OpenClient.getPlayerRunEnergy();
    }

    public static boolean isRunEnabled() {
        return Varps.getAt(173).getValue() == 1;
    }

    public static boolean toggleRun() {
        final boolean start = isRunEnabled();
        InterfaceComponent button =
            Interfaces.newQuery().containers(160).types(InterfaceComponent.Type.CONTAINER)
                .grandchildren(false).actions("Toggle Run").results().first();
        if ((button == null || !button.isVisible()) && ControlPanelTab.SETTINGS.open()) {
            button = Interfaces.newQuery()
                .containers(RemoteConstants.getAsInt("OSRS-Options-Tab-Widget"))
                .actions("Toggle Run").results().first();
        }
        return button != null && button.interact("Toggle Run") &&
            Execution.delayUntil(() -> start != isRunEnabled(), 1250, 2500);
    }
}
