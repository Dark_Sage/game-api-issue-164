package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.objects;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class DialogObjectVertex extends ObjectVertex implements SerializableVertex {
    private boolean click_continue;
    private int delay_length;
    private Pattern dialog_option;

    public DialogObjectVertex(
        int x, int y, int plane,
        Pattern name, Pattern action, Pattern dialog_option,
        boolean click_continue,
        int delay_length, Collection<WebRequirement> conditions
    ) {
        this(new Coordinate(x, y, plane), name, action, dialog_option, click_continue, delay_length,
            conditions
        );
    }

    public DialogObjectVertex(
        Coordinate position,
        Pattern name, Pattern action, Pattern dialog_option,
        boolean click_continue,
        int delay_length, Collection<WebRequirement> conditions
    ) {
        super(position, name, action, conditions);
        this.dialog_option = dialog_option;
        this.click_continue = click_continue;
        this.delay_length = delay_length;
    }

    public DialogObjectVertex(
        Coordinate position,
        Pattern name, Pattern action, Pattern dialog_option,
        boolean click_continue,
        int delay_length, Collection<WebRequirement> conditions,
        Collection<WebRequirement> blockingConditions
    ) {
        super(position, name, action, conditions, blockingConditions);
        this.dialog_option = dialog_option;
        this.click_continue = click_continue;
        this.delay_length = delay_length;
    }

    public DialogObjectVertex(
        int x, int y, int plane,
        String name, String action, Pattern dialog_option,
        boolean click_continue,
        int delay_length, Collection<WebRequirement> conditions
    ) {
        this(new Coordinate(x, y, plane), Regex.getPatternForExactString(name),
            Regex.getPatternForExactString(action), dialog_option, click_continue, delay_length,
            conditions
        );
    }

    public DialogObjectVertex(
        Coordinate position,
        String name, String action, Pattern dialog_option,
        boolean click_continue,
        int delay_length, Collection<WebRequirement> conditions
    ) {
        this(position, Regex.getPatternForExactString(name), Regex.getPatternForExactString(action),
            dialog_option, click_continue, delay_length, conditions
        );
    }

    public DialogObjectVertex(
        Coordinate position,
        String name, String action, Pattern dialog_option,
        boolean click_continue,
        int delay_length, Collection<WebRequirement> conditions,
        Collection<WebRequirement> blockingConditions
    ) {
        this(position, Regex.getPatternForExactString(name), Regex.getPatternForExactString(action),
            dialog_option, click_continue, delay_length, conditions, blockingConditions
        );
    }

    public DialogObjectVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    public int getDelayLength() {
        return delay_length;
    }

    public Pattern getDialogOption() {
        return dialog_option;
    }

    public boolean shouldClickContinue() {
        return click_continue;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getPosition()).append(getTargetPattern().pattern())
            .append(getActionPattern().pattern())
            .append(getDialogOption() != null ? getDialogOption().pattern() : getDialogOption())
            .append(click_continue)
            .append(getDelayLength()).toHashCode();
    }

    @Override
    public String toString() {
        Coordinate pos = getPosition();
        return "DialogObjectVertex(name=" + getTargetPattern() + ", action=" + getActionPattern() +
            ", dialogOption=" + getDialogOption() + ", clickContinue=" + shouldClickContinue() +
            ", delayLength=" + getDelayLength() + ", x=" + pos.getX() + ", y=" + pos.getY() +
            ", plane=" + pos.getPlane();
    }

    public int getOpcode() {
        return 9;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(target.pattern());
        stream.writeInt(target.flags());
        stream.writeUTF(action.pattern());
        stream.writeInt(action.flags());
        stream.writeUTF(dialog_option == null ? "null" : dialog_option.pattern());
        stream.writeInt(dialog_option == null ? 0 : dialog_option.flags());
        stream.writeUTF(Boolean.toString(shouldClickContinue()));
        stream.writeInt(0);
        stream.writeUTF("null");
        stream.writeInt(0);
        stream.writeInt(delay_length);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.target = Pattern.compile(stream.readUTF(), stream.readInt());
        this.action = Pattern.compile(stream.readUTF(), stream.readInt());
        String pattern = stream.readUTF();
        int flags = stream.readInt();
        this.dialog_option =
            Objects.equals("null", pattern) ? null : Pattern.compile(pattern, flags);
        this.click_continue = Boolean.valueOf(stream.readUTF());
        stream.readInt();
        stream.readUTF();
        stream.readInt();
        this.delay_length = stream.readInt();
        return true;
    }

    @Override
    public boolean step() {
        GameObject object = getObject();
        if (object != null && (object.isVisible() || Camera.turnTo(object)) &&
            object.interact(action, target)) {
            if (!Execution.delayUntil(
                () -> dialog_option != null && !ChatDialog.getOptions().isEmpty() ||
                    shouldClickContinue() && ChatDialog.getContinue() != null, 2000, 3000)) {
                return false;
            }
        } else {
            return false;
        }
        if (shouldClickContinue()) {
            ChatDialog.Continue chatContinue = ChatDialog.getContinue();
            while (chatContinue != null && chatContinue.isValid()) {
                chatContinue.select();
            }
        }
        ChatDialog.Option chatOption =
            dialog_option != null ? ChatDialog.getOption(dialog_option) : null;
        return chatOption == null || chatOption.select();
    }
}
