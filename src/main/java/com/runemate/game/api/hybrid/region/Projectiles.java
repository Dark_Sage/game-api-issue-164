package com.runemate.game.api.hybrid.region;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.region.*;
import java.util.function.*;

/**
 * For retrieval, sorting, and analysis of Projectiles
 */
public final class Projectiles {
    private Projectiles() {
    }

    public static ProjectileQueryBuilder newQuery() {
        return new ProjectileQueryBuilder();
    }

    public static LocatableEntityQueryResults<Projectile> getLoaded() {
        return getLoaded((Predicate<? super Projectile>) null);
    }

    public static LocatableEntityQueryResults<Projectile> getLoaded(final int... ids) {
        return getLoaded(projectile -> {
            final int projectileId = projectile.getSpotAnimationId();
            for (final int id : ids) {
                if (projectileId == id) {
                    return true;
                }
            }
            return false;
        });
    }

    public static LocatableEntityQueryResults<Projectile> getLoaded(
        final Predicate<? super Projectile> filter
    ) {
        return OSRSProjectiles.getLoaded(filter);
    }

    public static Predicate<Projectile> getModelPredicate(final int... acceptedHashes) {
        return projectile -> {
            final Model model = projectile.getModel();
            if (model != null) {
                final int hash = model.hashCode();
                for (int accepted : acceptedHashes) {
                    if (hash == accepted) {
                        return true;
                    }
                }
            }
            return false;
        };
    }
}
