package com.runemate.game.api.hybrid.entities;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.util.*;
import javax.annotation.*;

/**
 * An entity on the world-graph that is
 * {@link com.runemate.game.api.hybrid.entities.details.Locatable},
 * {@link com.runemate.game.api.hybrid.entities.details.Interactable},
 * {@link com.runemate.game.api.hybrid.util.Validatable}, and
 * {@link com.runemate.game.api.hybrid.entities.details.Renderable}
 */
public interface LocatableEntity extends Locatable, Interactable, Validatable, Renderable, Modeled {
    @Nullable
    @Override
    default Coordinate getPosition() {
        return getPosition(null);
    }

    @Nullable
    @Override
    default Coordinate.HighPrecision getHighPrecisionPosition() {
        return getHighPrecisionPosition(null);
    }

    @Nullable
    @Override
    default Area.Rectangular getArea() {
        return getArea(null);
    }

    /**
     * Gets the position using the region base as the coordinate to offset from. For optimization.
     *
     * @param regionBase The Coordinate base of the loaded region
     */
    @Nullable
    Coordinate getPosition(Coordinate regionBase);

    /**
     * Gets the high precision position using the region base as the coordinate to offset from. For optimization.
     *
     * @param regionBase The Coordinate base of the loaded region
     */
    @Nullable
    Coordinate.HighPrecision getHighPrecisionPosition(Coordinate regionBase);

    /**
     * Gets the area using the region base as the coordinate to offset from. For optimization.
     *
     * @param regionBase The Coordinate base of the loaded region
     */
    @Nullable
    Area.Rectangular getArea(Coordinate regionBase);
}
