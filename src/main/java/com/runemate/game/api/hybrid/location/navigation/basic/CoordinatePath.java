package com.runemate.game.api.hybrid.location.navigation.basic;

import com.google.common.collect.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.projection.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import javax.annotation.*;

public abstract class CoordinatePath extends Path {
    private int maxStepDeviation;
    private int maxMinimapStepDistance = 15;
    private int maxViewportStepDistance = 9;

    @Override
    public final Coordinate getNext() {
        return getNext(false);
    }

    public Coordinate getNext(boolean preferViewportTraversal) {
        List<Coordinate> backwards_path = Lists.reverse(getVertices());
        if (!backwards_path.isEmpty()) {
            Area.Rectangular region = Region.getArea();
            if (region != null) {
                if (preferViewportTraversal) {
                    Shape viewport = Projection.getViewport();
                    //Gets the last coordinate in the path that is at least 95% visible.
                    for (final Coordinate coordinate : backwards_path) {
                        if (coordinate.getVisibility(viewport) >= 95 &&
                            Distance.to(coordinate) <= maxViewportStepDistance) {
                            return coordinate;
                        }
                    }
                }
                if (false || Minimap.isInteractable()) {
                    for (final Coordinate coordinate : backwards_path) {
                        if (coordinate.minimap().isVisible() &&
                            Distance.to(coordinate) <= maxMinimapStepDistance) {
                            return coordinate;
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public final boolean step(@Nonnull TraversalOption... options) {
        boolean toggleRunBeforeStepping =
            PlayerSense.getAsBoolean(PlayerSense.Key.TOGGLE_RUN_BEFORE_TRAVERSING);
        if (toggleRunBeforeStepping && !triggerRun(options)) {
            return false;
        }
        if (isEligibleToStep(options)) {
            boolean useViewport = Arrays.asList(options).contains(TraversalOption.PREFER_VIEWPORT);
            Coordinate next = getNext(useViewport);
            if (next == null) {
                return false;
            }
            if (maxStepDeviation > 0) {
                next = next.randomize(maxStepDeviation, maxStepDeviation);
            }
            if (useViewport ? !next.isVisible() : !next.minimap().isVisible()) {
                return false;
            }
            if (useViewport ? !next.interact("Walk here") : !next.minimap().click()) {
                return false;
            }
        }
        return triggerStaminaEnhancement(options) &&
            (toggleRunBeforeStepping || triggerRun(options));
    }

    /**
     * Creates a new CoordinatePath which views the vertices in the opposite order.
     *
     * @return a new, reversed CoordinatePath
     */
    public final CoordinatePath reverse() {
        return PredefinedPath.create(Lists.reverse(getVertices()));
    }

    /**
     * Sets the random stepping offset to between 0 and max.
     * The stepping offset is the amount of deviation from the usual calculated point that will be clicked.
     * Setting this to a high value may result in the bot clicking a minimap button or even off of the minimap.
     *
     * @param max recommended to be around 1, but at most 4
     */
    public void setStepDeviation(int max) {
        this.maxStepDeviation = max;
    }

    /**
     * @see CoordinatePath#getMaxMinimapStepDistance
     */
    @Deprecated
    public int getMaxStepDistance() {
        return this.maxMinimapStepDistance;
    }

    /**
     * @see CoordinatePath#setMaxMinimapStepDistance
     */
    @Deprecated
    public void setMaxStepDistance(int max) {
        this.maxMinimapStepDistance = max;
    }

    public int getMaxMinimapStepDistance() {
        return this.maxMinimapStepDistance;
    }

    public void setMaxMinimapStepDistance(int max) {
        this.maxMinimapStepDistance = max;
    }

    public int getMaxViewportStepDistance() {
        return this.maxViewportStepDistance;
    }

    public void setMaxViewportStepDistance(int max) {
        this.maxViewportStepDistance = max;
    }

    @Override
    public abstract List<Coordinate> getVertices();
}
