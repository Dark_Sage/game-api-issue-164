package com.runemate.game.api.hybrid.local;

import com.google.common.cache.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.cache.*;
import com.runemate.game.cache.file.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;

/**
 * Used to extract a single piece of data from a Varp that has multiple pieces of information packed into it.
 * Used by the game in client scripts to simplify accessing information from packed varps.
 * Use these instead of manually shifting and masking Varps when possible.
 * Watch the real-time change table in the development toolkit.
 *
 * @see Varbits
 */
@Deprecated
public final class Varpbits {
    private static final Cache<Integer, Varpbit> CACHE =
        CacheBuilder.newBuilder().expireAfterAccess(1, TimeUnit.MINUTES).build();

    private static final VarbitLoader LOADER = new VarbitLoader();

    private Varpbits() {
    }

    /**
     * Gets a list of all varpbits
     */
    public static List<Varpbit> getAll() {
        Js5IndexFile archive = (JS5CacheController.getLargestJS5CacheController()).getIndexFile(2);
        if (archive != null) {
            Map<Integer, byte[]> entries = archive.getReferenceTable().load(14);
            ArrayList<Varpbit> varpbits = new ArrayList<>(entries.size());
            for (Map.Entry<Integer, byte[]> entry : entries.entrySet()) {
                try {
                    CacheVarbit parsed = (LOADER).form(14, entry.getKey(), entry.getValue());
                    if (parsed != null) {
                        varpbits.add(
                            new Varpbit(entry.getKey(), parsed.typeId, parsed.varIndex, parsed.msb,
                                parsed.lsb
                            ));
                    }
                } catch (final IOException ioe) {
                    ioe.printStackTrace();
                    System.err.println(
                        "Unable to load Varpbit at " + entry.getKey() + ": \"" + ioe.getMessage() + '"');
                }
            }
            return varpbits;
        }
        return Collections.emptyList();
    }

    /**
     * Gets the Varpbit at the specified index
     */
    public static Varpbit getAt(int index) {
        if (index >= 0) {
            Varpbit varpbits = CACHE.getIfPresent(index);
            if (varpbits != null) {
                return varpbits;
            }
            try {
                CacheVarbit parsed =
                    (LOADER).load(JS5CacheController.getLargestJS5CacheController(), 14, index);
                if (parsed != null) {
                    Varpbit vb =
                        new Varpbit(index, parsed.typeId, parsed.varIndex, parsed.msb, parsed.lsb);
                    CACHE.put(index, vb);
                    return vb;
                }
            } catch (final IOException ioe) {
                System.err.println(
                    "Unable to load Varpbit at " + index + ": \"" + ioe.getMessage() + '"');
            }
        }
        return null;
    }
}
