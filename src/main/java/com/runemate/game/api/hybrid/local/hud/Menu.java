package com.runemate.game.api.hybrid.local.hud;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.script.*;
import java.awt.*;
import java.util.List;
import java.util.regex.*;
import java.util.stream.*;
import javax.annotation.*;

/**
 * Used for interacting with the game's right-click context menu
 */
public final class Menu {

    private Menu() {
    }

    public static List<MenuItem> getItems() {
        final List<OpenMenuItem> items = OpenMenu.getItems();
        return items.stream().map(MenuItem::new).collect(Collectors.toList());
    }

    public static int getItemCount() {
        return OpenMenu.getSize();
    }


    public static boolean isOpen() {
        return OpenMenu.isOpen();
    }

    /**
     * An equivalent of new Point(getX(), getY()) that is slightly optimized
     */

    public static Point getPosition() {
        return new Point(OpenMenu.getX(), OpenMenu.getY());
    }

    /**
     * A convenient wrapper for new Rectangle(getX(), getY(), getWidth(), getHeight()) that is slightly optimized
     */

    public static Rectangle getBounds() {
        return new Rectangle(OpenMenu.getX(), OpenMenu.getY(), OpenMenu.getWidth(), OpenMenu.getHeight());
    }


    public static int getX() {
        return OpenMenu.getX();
    }


    public static int getY() {
        return OpenMenu.getY();
    }


    public static int getWidth() {
        return OpenMenu.getWidth();
    }


    public static int getHeight() {
        return OpenMenu.getHeight();
    }

    public static int indexOf(final String action) {
        return indexOf(action, null);
    }

    public static int indexOf(final String action, final String target) {
        Pattern actionPattern = null;
        if (action != null) {
            actionPattern = Regex.getPatternForExactString(action);
        }
        Pattern targetPattern = null;
        if (target != null) {
            targetPattern = Regex.getPatternForExactString(target);
        }
        return indexOf(actionPattern, targetPattern);
    }

    public static int indexOf(final Pattern action, final Pattern target) {
        final MenuItem item = getItem(action, target);
        return item != null ? item.getIndex() : -1;
    }

    public static int indexOf(@Nonnull final MenuItem item) {
        return item.getIndex();
    }

    public static boolean contains(final String action) {
        return contains(action != null ? Regex.getPatternForExactString(action) : null, null);
    }

    public static boolean contains(final String action, final String target) {
        return contains(
            action != null ? Regex.getPatternForExactString(action) : null,
            target != null ? Regex.getPatternForExactString(target) : null
        );
    }

    public static boolean contains(final Pattern action, final Pattern target) {
        if (action == null && target == null) {
            return getItemCount() > 0;
        }
        for (final MenuItem item : getItems()) {
            if (item.targets(null, action, target)) {
                return true;
            }
        }
        return false;
    }

    @Nullable
    public static MenuItem getItemAt(final int index) {
        final List<MenuItem> items = getItems();
        return index < items.size() ? items.get(index) : null;
    }

    @Nullable
    public static MenuItem getItem(final String action) {
        return getItem(action != null ? Regex.getPatternForExactString(action) : null, null);
    }

    @Nullable
    public static MenuItem getItem(final Pattern action) {
        return getItem(action, null);
    }

    @Nullable
    public static MenuItem getItem(final Interactable scenetarget, final Pattern action) {
        return getItem(scenetarget, action, null);
    }

    @Nullable
    public static MenuItem getItem(final String action, final String target) {
        return getItem(
            action != null ? Regex.getPatternForExactString(action) : null,
            target != null ? Regex.getPatternForExactString(target) : null
        );
    }

    @Nullable
    public static MenuItem getItem(final Pattern action, final Pattern target) {
        return getItem(null, action, target);
    }

    @Nullable
    public static MenuItem getItem(final Interactable scenetarget, final Pattern action, final Pattern target) {
        return getItems().stream().filter(item -> item.targets(scenetarget, action, target))
            .findAny()
            .orElse(null);
    }


    public static boolean open() {
        return OpenMenu.isOpen() || Mouse.click(Mouse.Button.RIGHT) && Execution.delayUntil(
            OpenMenu::isOpen, 150, 300);
    }


    public static boolean close() {
        if (!OpenMenu.isOpen()) {
            return true;
        }
        final MenuItem item = getItem("Cancel");
        return item != null && item.click() && Execution.delayUntil(
            () -> !OpenMenu.isOpen(), 150, 300);
    }

    public static boolean click(final Interactable entity, final String action) {
        return click(entity, action, null);
    }

    public static boolean click(final Interactable entity, final Pattern action) {
        return click(entity, action, null);
    }

    public static boolean click(final String action, String target) {
        return click(null, action, target);
    }

    public static boolean click(final Pattern action, Pattern target) {
        fixOSRSItemSelectionGlitch();
        MenuItem item = getInteractionMenuItem(null, action, target);
        return item != null && item.click();
    }

    public static boolean click(final Interactable entity, final String action, final String target) {
        Pattern actionPattern = null;
        if (action != null) {
            actionPattern = Regex.getPatternForExactString(action);
        }
        Pattern targetPattern = null;
        if (target != null) {
            targetPattern = Regex.getPatternForExactString(target);
        }
        return click(entity, actionPattern, targetPattern);
    }

    public static boolean click(Interactable entity, final Pattern action, final Pattern target) {
        if (entity == null) {
            return click(action, target);
        }
        if (entity instanceof MenuItem) {
            throw new IllegalArgumentException("Menu.click cannot accept a MenuItem as its target entity.");
        }
        if (entity instanceof Model) {
            entity = ((Model) entity).owner;
        }
        fixOSRSItemSelectionGlitch();
        MenuItem item = null;
        if (!entity.isHovered()
            || (item = getInteractionMenuItem(entity, action, target)) == null) {
            if (Menu.isOpen()) {
                Menu.close();
            }
            entity.hover();
        }
        if (item == null) {
            item = getInteractionMenuItem(entity, action, target);
        }
        return item != null && item.click(entity);
    }

    private static void fixOSRSItemSelectionGlitch() {
        if (Environment.isOSRS() && Inventory.isItemSelected()
            && Inventory.getSelectedItem() == null) {
            Environment.getLogger().info(
                "[OSRS Glitch] The OSRS game engine has a glitch when using consumable items on entities. This breaks most bots, so we're correcting things.");
            //Fix glitch where it "selects" an item and consumes it in one go
            InteractableRectangle slot = Random.nextElement(Inventory.getSlotBounds());
            if (slot != null && slot.click()) {
                Execution.delayUntil(
                    () -> !Inventory.isItemSelected() || Inventory.getSelectedItem() != null, 300,
                    600
                );
            }
        }
    }

    private static MenuItem getInteractionMenuItem(final Interactable entity, final Pattern action, final Pattern target) {
        MenuItem item;
        if (entity instanceof Model) {
            if (((Model) entity).owner instanceof Entity) {
                item = getItem(((Model) entity).owner, action, target);
            } else {
                item = getItem(action, target);
            }
        } else if (entity instanceof InterfaceComponent) {
            item = getItem(entity, action, target);
            if (item == null) {
                item = getItem(action, target);
            }
        } else {
            item = getItem(entity, action, target);
        }
        return item;
    }
}
