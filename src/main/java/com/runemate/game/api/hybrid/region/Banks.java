package com.runemate.game.api.hybrid.region;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import java.util.*;
import java.util.function.*;

public final class Banks {
    private static final Predicate<Npc> BANKER_FILTER = Npcs.getActionPredicate("Bank");
    private static final Predicate<GameObject> BANK_CHEST_FILTER =
        GameObjects.getNamePredicate("Bank chest", "Shantay chest")
            .and(GameObjects.getActionPredicate());
    private static final Predicate<GameObject> BANK_BOOTH_FILTER =
        GameObjects.getActionPredicate("Bank");
    private static final Coordinate[] RS3_BANK_BLACKLIST = {
        new Coordinate(3180, 3433, 0),
        new Coordinate(3191, 3445, 0),
        new Coordinate(3148, 3448, 0),
        new Coordinate(3148, 3449, 0)
    };
    private static final Coordinate[] OSRS_BANK_BLACKLIST = {
        new Coordinate(2726, 3496, 0),
        new Coordinate(2729, 3496, 0),
        new Coordinate(3088, 3242, 0),
        new Coordinate(3147, 3448, 0),
        new Coordinate(3148, 3448, 0),
        new Coordinate(3148, 3449, 0),
        new Coordinate(3180, 3433, 0),
        new Coordinate(3187, 3446, 0),
        new Coordinate(3191, 3445, 0),
        new Coordinate(3096, 3492, 0)
    };

    private Banks() {
    }

    public static BankQueryBuilder newQuery() {
        return new BankQueryBuilder();
    }

    /**
     * Gets all loaded bankers, bank booths, and bank chests (deposit boxes are not included)
     */
    public static LocatableEntityQueryResults<LocatableEntity> getLoaded() {
        return getLoaded(null);
    }

    public static LocatableEntityQueryResults<LocatableEntity> getLoaded(
        final Predicate<? super LocatableEntity> filter
    ) {
        final List<LocatableEntity> banks = new ArrayList<>(getLoadedBankBooths());
        banks.addAll(getLoadedBankChests());
        banks.addAll(getLoadedBankers());
        if (filter != null) {
            banks.removeIf(filter.negate());
        }
        return new LocatableEntityQueryResults<>(banks);
    }

    public static LocatableEntityQueryResults<Npc> getLoadedBankers() {
        return Npcs.newQuery().actions("Bank")
            .off(OSRS_BANK_BLACKLIST)
            .results();
    }

    public static LocatableEntityQueryResults<GameObject> getLoadedBankBooths() {
        return GameObjects.newQuery().types(GameObject.Type.PRIMARY).actions("Bank")
            .off(OSRS_BANK_BLACKLIST)
            .results();
    }

    public static LocatableEntityQueryResults<GameObject> getLoadedBankChests() {
        return GameObjects.newQuery().types(GameObject.Type.PRIMARY)
            .names("Bank chest", "Shantay chest", "Bank boat").actions("Use", "Open")
            .off(OSRS_BANK_BLACKLIST)
            .results();
    }

    @Deprecated
    public static Predicate<Npc> getBankerPredicate() {
        return BANKER_FILTER;
    }

    @Deprecated
    public static Predicate<GameObject> getBankBoothPredicate() {
        return BANK_BOOTH_FILTER;
    }

    @Deprecated
    public static Predicate<GameObject> getBankChestPredicate() {
        return BANK_CHEST_FILTER;
    }
}
