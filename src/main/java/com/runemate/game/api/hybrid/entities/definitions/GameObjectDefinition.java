package com.runemate.game.api.hybrid.entities.definitions;

import com.google.common.cache.*;
import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.api.hybrid.cache.materials.*;
import com.runemate.game.api.hybrid.entities.attributes.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.internal.exception.*;
import com.runemate.game.cache.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import javax.annotation.*;

/**
 * The definition of an in-game object
 */
public abstract class GameObjectDefinition implements Onymous, Identifiable {

    private static final Cache<Integer, GameObjectDefinition> RS3_CACHE =
        CacheBuilder.newBuilder().maximumSize(10000).expireAfterAccess(1, TimeUnit.MINUTES).build();
    private static final Cache<Integer, GameObjectDefinition> OSRS_CACHE =
        CacheBuilder.newBuilder().maximumSize(10000).expireAfterAccess(1, TimeUnit.MINUTES).build();
    private static final ObjectDefinitionLoader osrsDefLoader = new ObjectDefinitionLoader(2);

    /**
     * Gets a list of definitions within the range of [first, last]
     */
    @Nonnull
    public static List<GameObjectDefinition> get(final int first, final int last) {
        return get(first, last, null);
    }

    /**
     * Gets a list of definitions within the range of [first, last] that are accepted by the filter
     */
    @Nonnull
    public static List<GameObjectDefinition> get(final int first, final int last, final Predicate<GameObjectDefinition> filter) {
        ArrayList<GameObjectDefinition> definitions = new ArrayList<>(last - first + 1);
        for (int id = first; id <= last; ++id) {
            final GameObjectDefinition definition = get(false, id);
            if (definition != null && (filter == null || filter.test(definition))) {
                definitions.add(definition);
            }
        }
        definitions.trimToSize();
        return definitions;
    }

    @Nullable
    private static GameObjectDefinition get(final boolean cache, final int id) {
        return get(JS5CacheController.getLargestJS5CacheController(), false, cache, id);
    }

    @Nullable
    private static GameObjectDefinition get(JS5CacheController cache, boolean rs3, final boolean storeInCache, final int id) {
        if (id >= 0) {
            try {
                GameObjectDefinition def = OSRS_CACHE.getIfPresent(id);
                if (def != null) {
                    return def;
                }
                CacheObjectDefinition bdef = osrsDefLoader.load(ConfigType.OBJECT, id);
                if (bdef != null) {
                    def = bdef.extended();
                    if (storeInCache) {
                        OSRS_CACHE.put(id, def);
                    }
                    return def;
                }
            } catch (final Exception ioe) {
                throw new UnableToParseBufferException("Unable to load object definition for " + id + ": \"" + ioe.getMessage() + '"', ioe);
            }
        }
        return null;
    }

    /**
     * Gets the definition for the object with the specified id
     *
     * @return The definition if available, otherwise null
     */
    @Nullable
    public static GameObjectDefinition get(final int id) {
        return get(true, id);
    }

    /**
     * Loads all definitions
     */
    @Nonnull
    public static List<GameObjectDefinition> loadAll() {
        return loadAll(null);
    }

    /**
     * Loads all definitions that are accepted by the filter
     */
    @Nonnull
    public static List<GameObjectDefinition> loadAll(final Predicate<GameObjectDefinition> filter) {
        int quantity = osrsDefLoader.getFiles(JS5CacheController.getLargestJS5CacheController(), 6).length;
        ArrayList<GameObjectDefinition> definitions = new ArrayList<>(quantity);
        for (int id = 0; id <= quantity; ++id) {
            final GameObjectDefinition definition = get(false, id);
            if (definition != null && (filter == null || filter.test(definition))) {
                definitions.add(definition);
            }
        }
        definitions.trimToSize();
        return definitions;
    }

    /**
     * Gets the GameObjectDefinition containing data for this objects current state (transformation).
     * For example, in farming the patch id doesn't change when your player plants something in it,
     * however it's local state does. This GameObjectDefinition contains information such as
     * a plant specific id, the plants specific name, and the available powers.
     *
     * @return a GameObjectDefinition if the objects state changes locally, otherwise null
     */
    @Nullable
    public abstract GameObjectDefinition getLocalState();

    /**
     * Gets all the possible transformations for the given game object.
     * Objects can transform based on various in game events such as activating a lodestone.
     *
     * @return a non-null Collection of all possible transformations.
     */
    @Nonnull
    public abstract Collection<GameObjectDefinition> getTransformations();

    @Nonnull
    @Override
    public abstract String getName();

    /**
     * Gets the powers that can be performed on the object
     */
    @Nonnull
    public abstract List<String> getActions();

    /**
     * Gets a list of all possible animations that can be performed by this object
     */
    @Nonnull
    public abstract List<Integer> getAnimationIds();

    /**
     * Gets a list of integers that are used to represent this entities appearance.
     * Internally these are used to generate a GameObject's Model.
     */
    @Nonnull
    public abstract List<Integer> getAppearance();

    /**
     * Gets the objects clipping type, used to calculate collisions
     */
    public abstract int getClippingType();

    /**
     * Gets the objects width (in tiles)
     */
    public abstract int getWidth();

    /**
     * Gets the objects height (in tiles)
     */
    public abstract int getHeight();

    /**
     * A flag that is used to determine whether or not certain objects can be walked on.
     * Its main purpose is for path generation.
     */
    public abstract boolean impassable();

    /**
     * Gets the attribute with the specified id
     *
     * @return the attribute if available, otherwise null
     */
    @Nullable
    public abstract Attribute getAttribute(final long id);

    /**
     * Gets a list of all of the objects attributes.
     */
    public abstract List<Attribute> getAttributes();

    /**
     * Gets a mapping of colors that are to be substituted in the base model.
     */
    public abstract Map<Color, Color> getColorSubstitutions();

    /**
     * Gets a mapping of materials that are to be substituted in the base model.
     */
    public abstract Map<Material, Material> getMaterialSubstitutions();

    @Override
    public String toString() {
        return "GameObjectDefinition(id: " + getId() + ", name: " + getName() + ')';
    }

    /**
     * Gets an identifier used to identify the object
     */
    public abstract int getId();

    /**
     * The circular icons that are displayed on the map and minimap as an indicator
     */
    public abstract int getMapFunction();

    /**
     * Object icons that are rendered on the map and minimap such as trees, rocks, bushes, etc.
     */
    public abstract int getMapScene();

    public abstract int[] getModelTypes();

    /**
     * Determines whether or not projectiles can bypass/go through the object to reach their targets
     *
     * @return
     */
    public abstract boolean impenetrable();

    public abstract int getModelXScale();

    public abstract int getModelYScale();

    public abstract int getModelZScale();

    public abstract int getModelXTranslation();

    public abstract int getModelYTranslation();

    public abstract int getModelZTranslation();

    public abstract boolean isModelMirrorable();

    public abstract boolean isInteractable();

    //public abstract int getInset();
}
