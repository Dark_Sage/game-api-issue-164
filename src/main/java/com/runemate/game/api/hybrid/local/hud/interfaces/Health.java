package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.annotations.*;

public final class Health {
    private Health() {
    }

    public static int getCurrent() {
        return OSRSHealth.getCurrent();
    }

    public static int getMaximum() {
        return OSRSHealth.getMaximum();
    }

    public static int getCurrentPercent() {
        int current = getCurrent();
        if (current < 0) {
            return 100;
        }
        int maximum = getMaximum();
        if (maximum < 0) {
            return 100;
        }
        if (maximum == 0) {
            return 0;
        }
        return (current * 100) / maximum;
    }

    public static boolean isPoisoned() {
        return Varps.getAt(102).getValue() > 0;
    }

    @OSRSOnly
    public static boolean isVenomous() {
        return Varps.getAt(102).getValue() >= 1000000;
    }

    /**
     * Whether your player is suffering from a disease due to combat around Jiggig on OSRS
     */
    @OSRSOnly
    public static boolean isDiseased() {
        return Varps.getAt(456).getValue() > 0;
    }
}
