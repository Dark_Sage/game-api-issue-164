package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.queries.results.*;

public abstract class InteractableQueryBuilder<T extends Interactable, QB extends QueryBuilder, QR extends InteractableQueryResults<T, QR>>
    extends QueryBuilder<T, QB, QR> {
    private Boolean visible;

    public QB visible() {
        visible = true;
        return get();
    }

    public QB invisible() {
        visible = false;
        return get();
    }

    @Override
    public boolean accepts(T argument) {
        return (visible == null || visible == argument.isVisible()) && super.accepts(argument);
        //return !(visible != null && visible != argument.isVisible()) && super.accepts(argument);
    }
}
