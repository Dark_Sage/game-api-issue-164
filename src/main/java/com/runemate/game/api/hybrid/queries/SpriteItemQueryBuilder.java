package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class SpriteItemQueryBuilder
    extends InteractableQueryBuilder<SpriteItem, SpriteItemQueryBuilder, SpriteItemQueryResults> {
    private final Inventories.Documented source;
    private int[] ids, indices, modelIds, maleModelIds, femaleModelIds, maleHeadModelIds,
        femaleHeadModelIds;
    private Pattern[] names, actions;
    private Boolean noted, placeholder = false, equipable, stacks;

    public SpriteItemQueryBuilder(final Inventories.Documented source) {
        this.source = source;
    }

    public SpriteItemQueryBuilder ids(final int... ids) {
        this.ids = ids;
        return get();
    }

    @Override
    public SpriteItemQueryBuilder get() {
        return this;
    }

    @Override
    public Callable<List<? extends SpriteItem>> getDefaultProvider() {
        return () -> Inventories.lookup(source).asList();
    }

    @Override
    protected SpriteItemQueryResults results(
        Collection<? extends SpriteItem> entries,
        ConcurrentMap<String, Object> cache
    ) {
        return new SpriteItemQueryResults(entries, cache);
    }

    public SpriteItemQueryBuilder indices(final int... indices) {
        this.indices = indices;
        return get();
    }

    public SpriteItemQueryBuilder groundModels(final int... modelIds) {
        this.modelIds = modelIds;
        return get();
    }

    public SpriteItemQueryBuilder maleModels(final int... modelIds) {
        this.maleModelIds = modelIds;
        return get();
    }

    public SpriteItemQueryBuilder femaleModels(final int... modelIds) {
        this.femaleModelIds = modelIds;
        return get();
    }

    public SpriteItemQueryBuilder maleHeadModels(final int... modelIds) {
        this.maleHeadModelIds = modelIds;
        return get();
    }

    public SpriteItemQueryBuilder femaleHeadModels(final int... modelIds) {
        this.femaleHeadModelIds = modelIds;
        return get();
    }

    public final SpriteItemQueryBuilder names(final String... names) {
        return names(Regex.getPatternsForExactStrings(names).toArray(new Pattern[names.length]));
    }

    public final SpriteItemQueryBuilder names(final Pattern... names) {
        this.names = names;
        return get();
    }

    public final SpriteItemQueryBuilder names(final Collection<Pattern> names) {
        return names(names.toArray(new Pattern[0]));
    }

    public final SpriteItemQueryBuilder actions(final String... actions) {
        return actions(
            Regex.getPatternsForExactStrings(actions).toArray(new Pattern[actions.length]));
    }

    public final SpriteItemQueryBuilder actions(final Pattern... actions) {
        this.actions = actions;
        return get();
    }

    public final SpriteItemQueryBuilder noted() {
        this.noted = true;
        return get();
    }

    public final SpriteItemQueryBuilder unnoted() {
        this.noted = false;
        return get();
    }

    public final SpriteItemQueryBuilder equipable(boolean yes) {
        this.equipable = yes;
        return get();
    }

    public final SpriteItemQueryBuilder stacks(boolean yes) {
        this.stacks = yes;
        return get();
    }

    public final SpriteItemQueryBuilder placeholder(boolean yes) {
        this.placeholder = yes;
        return get();
    }

    @Override
    public boolean accepts(SpriteItem argument) {
        boolean condition;
        if (ids != null) {
            condition = false;
            int id = argument.getId();
            for (final int value : ids) {
                if (value == id) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (indices != null) {
            condition = false;
            int index = argument.getIndex();
            for (final int value : indices) {
                if (value == index) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        ItemDefinition definition = null;
        if (placeholder != null) {
            if (definition == null) {
                definition = argument.getDefinition();
            }
            condition = false;
            if (definition != null) {
                condition = placeholder == definition.isPlaceholder();
            }
            if (!condition) {
                return false;
            }
        }
        if (stacks != null) {
            condition = false;
            if (definition == null) {
                definition = argument.getDefinition();
            }
            if (definition != null) {
                condition = stacks == definition.stacks();
            }
            if (!condition) {
                return false;
            }
        }
        if (noted != null) {
            condition = false;
            if (definition == null) {
                definition = argument.getDefinition();
            }
            if (definition != null) {
                condition = noted == definition.isNoted();
            }
            if (!condition) {
                return false;
            }
        }
        if (equipable != null) {
            if (definition == null) {
                definition = argument.getDefinition();
            }
            condition = false;
            if (definition != null) {
                condition = equipable == definition.isEquipable();
            }
            if (!condition) {
                return false;
            }
        }
        if (modelIds != null) {
            if (definition == null) {
                definition = argument.getDefinition();
            }
            condition = false;
            if (definition != null) {
                int model = definition.getGroundModelId();
                for (int modelId : modelIds) {
                    if (model == modelId) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (maleModelIds != null) {
            if (definition == null) {
                definition = argument.getDefinition();
            }
            condition = false;
            if (definition != null) {
                List<Integer> modelIds = definition.getMaleModelIds();
                for (final int value : this.maleModelIds) {
                    for (final int value2 : modelIds) {
                        if (value == value2) {
                            condition = true;
                            break;
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (femaleModelIds != null) {
            if (definition == null) {
                definition = argument.getDefinition();
            }
            condition = false;
            if (definition != null) {
                List<Integer> modelIds = definition.getFemaleModelIds();
                for (final int value : this.femaleModelIds) {
                    for (final int value2 : modelIds) {
                        if (value == value2) {
                            condition = true;
                            break;
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (maleHeadModelIds != null) {
            if (definition == null) {
                definition = argument.getDefinition();
            }
            condition = false;
            if (definition != null) {
                List<Integer> modelIds = definition.getMaleHeadModelIds();
                for (final int value : this.maleHeadModelIds) {
                    for (final int value2 : modelIds) {
                        if (value == value2) {
                            condition = true;
                            break;
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (femaleHeadModelIds != null) {
            if (definition == null) {
                definition = argument.getDefinition();
            }
            condition = false;
            if (definition != null) {
                List<Integer> modelIds = definition.getFemaleHeadModelIds();
                for (final int value : this.femaleHeadModelIds) {
                    for (final int value2 : modelIds) {
                        if (value == value2) {
                            condition = true;
                            break;
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (names != null) {
            if (definition == null) {
                definition = argument.getDefinition();
            }
            condition = false;
            if (definition != null) {
                String name = definition.getName();
                for (final Pattern value : this.names) {
                    if (value.matcher(name).find()) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (actions != null) {
            if (definition == null) {
                definition = argument.getDefinition();
            }
            condition = false;
            if (definition != null) {
                List<String> actions = definition.getInventoryActions();
                for (final String string : actions) {
                    for (final Pattern pattern : this.actions) {
                        if (pattern.matcher(string).find()) {
                            condition = true;
                            break;
                        }
                    }
                }
                if (!condition && Inventories.Documented.EQUIPMENT.equals(source)) {
                    actions = definition.getWornActions();
                    for (final String string : actions) {
                        for (final Pattern pattern : this.actions) {
                            if (pattern.matcher(string).find()) {
                                condition = true;
                                break;
                            }
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        return super.accepts(argument);
    }
}
