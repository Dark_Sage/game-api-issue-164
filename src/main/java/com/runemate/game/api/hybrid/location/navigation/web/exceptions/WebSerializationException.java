package com.runemate.game.api.hybrid.location.navigation.web.exceptions;

public class WebSerializationException extends RuntimeException {
    public WebSerializationException(String message) {
        super(message);
    }
}
