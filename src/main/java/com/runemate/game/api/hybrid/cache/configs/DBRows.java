package com.runemate.game.api.hybrid.cache.configs;

import com.google.common.cache.*;
import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.cache.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import lombok.experimental.*;

@UtilityClass
public class DBRows {

    private static final DBRowLoader LOADER = new DBRowLoader();
    private static final Cache<Integer, DBRow> CACHE = CacheBuilder.newBuilder()
        .maximumSize(100)
        .expireAfterAccess(5, TimeUnit.MINUTES)
        .build();

    public static DBRow load(int row) {
        return load(JS5CacheController.getLargestJS5CacheController(), row);
    }

    public static DBRow load(JS5CacheController controller, int row) {
        if (row >= 0) {
            var def = CACHE.getIfPresent(row);
            if (def == null) {
                try {
                    def = LOADER.load(controller, ConfigType.DBROW, row);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (def != null) {
                CACHE.put(row, def);
            }
            return def;
        }
        return null;
    }

    public static List<DBRow> loadAll() {
        return loadAll(null);
    }

    public static List<DBRow> loadAll(final Predicate<DBRow> filter) {
        var controller = JS5CacheController.getLargestJS5CacheController();
        var quantity = LOADER.getFiles(controller, ConfigType.DBROW).length;
        var tables = new ArrayList<DBRow>(quantity);
        for (int id = 0; id <= quantity; id++) {
            var loaded = load(controller, id);
            if (loaded != null && (filter == null || filter.test(loaded))) {
                tables.add(loaded);
            }
        }
        tables.trimToSize();
        return tables;
    }
}
