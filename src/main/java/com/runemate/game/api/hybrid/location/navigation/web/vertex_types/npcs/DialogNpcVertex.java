package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.npcs;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import java.io.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class DialogNpcVertex extends NpcVertex implements SerializableVertex {
    private Pattern name;
    private Pattern action;
    private double movement_radius;
    private boolean click_continue;
    private int delay_length;
    private Pattern dialog_option;

    public DialogNpcVertex(
        int x, int y, int plane, double movement_radius,
        Pattern name, Pattern action, Pattern dialog_option,
        boolean click_continue, int delay_length,
        Collection<WebRequirement> conditions
    ) {
        this(new Coordinate(x, y, plane), movement_radius, name, action, dialog_option,
            click_continue, delay_length, conditions
        );
    }

    public DialogNpcVertex(
        Coordinate position, double movement_radius,
        Pattern name, Pattern action, Pattern dialog_option,
        boolean click_continue, int delay_length,
        Collection<WebRequirement> conditions
    ) {
        super(position, conditions);
        this.movement_radius = movement_radius;
        this.name = name;
        this.action = action;
        this.dialog_option = dialog_option;
        this.click_continue = click_continue;
        this.delay_length = delay_length;
    }

    public DialogNpcVertex(
        int x, int y, int plane, double movement_radius,
        String name, String action, Pattern dialog_option,
        boolean click_continue, int delay_length,
        Collection<WebRequirement> conditions
    ) {
        this(new Coordinate(x, y, plane), movement_radius, Regex.getPatternForExactString(name),
            Regex.getPatternForExactString(action), dialog_option, click_continue, delay_length,
            conditions
        );
    }

    public DialogNpcVertex(
        Coordinate position, double movement_radius,
        String name, String action, Pattern dialog_option,
        boolean click_continue, int delay_length,
        Collection<WebRequirement> conditions
    ) {
        this(position, movement_radius, Regex.getPatternForExactString(name),
            Regex.getPatternForExactString(action), dialog_option, click_continue, delay_length,
            conditions
        );
    }

    public DialogNpcVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    public Pattern getAction() {
        return action;
    }

    public int getDelayLength() {
        return delay_length;
    }

    public Pattern getDialogOption() {
        return dialog_option;
    }

    public boolean shouldClickContinue() {
        return click_continue;
    }

    public double getMovementRadius() {
        return movement_radius;
    }

    public Pattern getName() {
        return name;
    }

    @Override
    public Npc getNpc() {
        final NpcQueryBuilder builder = Npcs.newQuery();
        if (movement_radius == 0) {
            builder.on(getPosition());
        } else {
            builder.within(new Area.Circular(getPosition(), movement_radius));
        }
        if (name != null) {
            builder.names(name);
        }
        if (action != null) {
            builder.actions(action);
        }
        if (provider != null) {
            builder.provider(() -> provider);
        }
        return builder.results().first();
    }

    @Override
    public void invalidateCache() {
        provider = null;
    }

    @Override
    public Predicate<Npc> getFilter() {
        final NpcQueryBuilder builder = Npcs.newQuery();
        if (movement_radius == 0) {
            builder.on(getPosition());
        } else {
            builder.within(new Area.Circular(getPosition(), movement_radius));
        }
        if (name != null) {
            builder.names(name);
        }
        if (action != null) {
            builder.actions(action);
        }
        return builder::accepts;
    }

    @Override
    public int getOpcode() {
        return 8;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeDouble(movement_radius);
        stream.writeUTF(name.pattern());
        stream.writeInt(name.flags());
        stream.writeUTF(action.pattern());
        stream.writeInt(action.flags());
        stream.writeUTF(dialog_option == null ? "null" : dialog_option.pattern());
        stream.writeInt(dialog_option == null ? 0 : dialog_option.flags());
        stream.writeUTF(Boolean.toString(click_continue));
        stream.writeInt(0);
        stream.writeUTF("null");
        stream.writeInt(0);
        stream.writeInt(delay_length);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.walkingBounds = new Area.Circular(position, movement_radius = stream.readDouble());
        this.name = Pattern.compile(stream.readUTF(), stream.readInt());
        this.action = Pattern.compile(stream.readUTF(), stream.readInt());
        String pattern = stream.readUTF();
        int flags = stream.readInt();
        this.dialog_option =
            Objects.equals("null", pattern) ? null : Pattern.compile(pattern, flags);
        this.click_continue = Boolean.valueOf(stream.readUTF());
        stream.readInt();
        stream.readUTF();
        stream.readInt();
        this.delay_length = stream.readInt();
        return true;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getPosition()).append(getName().pattern())
            .append(getAction().pattern())
            .append(getDialogOption() != null ? getDialogOption().pattern() : getDialogOption())
            .append(shouldClickContinue())
            .append(getDelayLength()).toHashCode();
    }

    @Override
    public String toString() {
        Coordinate pos = getPosition();
        return "DialogNpcVertex(name=" + getName() + ", action=" + getAction() +
            ", dialogOption=" + getDialogOption() + ", clickContinue=" + shouldClickContinue() +
            ", delayLength=" + getDelayLength() + ", x=" + pos.getX() + ", y=" + pos.getY() +
            ", plane=" + pos.getPlane();
    }

    @Override
    public boolean step() {
        boolean success = false;
        ChatDialog.Continue chatContinue;
        ChatDialog.Option chatOption =
            dialog_option != null ? ChatDialog.getOption(dialog_option) : null;
        if (chatOption != null) {
            success = chatOption.select();
        } else if (shouldClickContinue() && (chatContinue = ChatDialog.getContinue()) != null) {
            success = chatContinue.select();
        } else {
            Npc npc = getNpc();
            if (npc != null && npc.isVisible() && npc.interact(action, name)) {
                return Execution.delayUntil(
                    () -> dialog_option != null && !ChatDialog.getOptions().isEmpty() ||
                        shouldClickContinue() && ChatDialog.getContinue() != null, 2000, 3000);
            }
        }
        return Execution.delay(delay_length) && success;
    }
}
