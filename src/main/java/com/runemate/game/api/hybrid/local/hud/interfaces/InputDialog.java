package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import java.util.regex.*;

/**
 * <p>
 * Hybrid helper class for the "Enter amount:" dialog seen in banks, deposit boxes and elsewhere
 * <p>
 * Searches for EnterTextDialog for titles matching any of the following: "Enter amount", "Enter name", "How many ", "Set a price"
 **/
public class InputDialog {

    private final static int OSRS_WIDGET = 162;

    private final static Pattern ENTER_AMOUNT_PATTERN =
        Regex.getPatternContainingOneOf("Enter amount", "Enter name", "How many", "Set a price", "How much");

    private InputDialog() {
    }

    /**
     * Determines whether or not the "Enter amount:" dialog is open
     *
     * @return true if the dialog is open
     */
    public static boolean isOpen() {
        return getTextEntryComponent() != null;
    }

    /**
     * Enters the amount specified into the dialog box and delays until the action was successful
     *
     * @param amount amount to type
     * @return if successfully typed the amount, selected enter and confirmed the dialog has closed
     */
    public static boolean enterAmount(final int amount) {
        return enterAmount(amount, PlayerSense.getAsBoolean(PlayerSense.Key.USE_METRIC_FORMAT));
    }

    /**
     * @param amount   amount to type
     * @param metric true to force truncation of amount, false to type whole number
     * @return if successfully typed the amount, selected enter and confirmed the dialog has closed
     */
    public static boolean enterAmount(final int amount, final boolean metric) {
        return enterText(metric ? StringFormat.metricFormat(amount) : String.valueOf(amount), true);
    }

    /**
     * Enters the string specified into the dialog box and delays until the action was successful
     *
     * @param text       string to type
     * @param pressEnter press enter after typing the string
     * @return if successfully typed the amount, selected enter and confirmed the dialog has closed
     */
    public static boolean enterText(final String text, final boolean pressEnter) {
        return isOpen() && Keyboard.type(text, pressEnter) &&
            Execution.delayWhile(InputDialog::isOpen, 1000, 1200);
    }

    /**
     * @param text string to type
     * @return if successfully typed the amount, selected enter and confirmed the dialog has closed
     */
    public static boolean enterText(final String text) {
        return enterText(text, true);
    }

    private static InterfaceComponent getTextEntryComponent() {
        return Interfaces.newQuery().containers(OSRS_WIDGET)
            .texts(ENTER_AMOUNT_PATTERN)
            .grandchildren(false).types(InterfaceComponent.Type.LABEL).visible()
            .results().first();
    }
}
