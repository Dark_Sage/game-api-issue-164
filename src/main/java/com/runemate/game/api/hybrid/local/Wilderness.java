package com.runemate.game.api.hybrid.local;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import java.util.regex.*;

public class Wilderness {
   
    private final static Area WILDERNESS_1 = new Area.Rectangular(new Coordinate(2944, 3520, 0), new Coordinate(3391, 4351, 0));
    private final static Area WILDERNESS_2 = new Area.Rectangular(new Coordinate(2944, 3520, 0), new Coordinate(3007, 3583, 0));
    private final static Area WILDERNESS_3 = new Area.Rectangular(new Coordinate(2944, 3520, 0), new Coordinate(3455, 4479, 0));
            /**
             * Ported from the logic provided in [proc,wilderness_level]()(int)
             * <p>
             * [proc,wilderness_level]()(int)
             * def_int $int0 = 0;
             * if (~inzone(0_46_55_0_0, 3_52_67_63_63, coord) = 1) {
             * $int0 = calc((coordz(coord) - 55 * 64) / 8 + 1);
             * } else if (~inzone(0_47_158_0_0, 3_47_158_63_63, coord) = 1) {
             * $int0 = calc((coordz(coord) - 155 * 64) / 8 - 1);
             * } else if (~inzone(0_46_155_0_0, 3_53_169_63_63, coord) = 1) {
             * $int0 = calc((coordz(coord) - 155 * 64) / 8 + 1);
             * }
             * return($int0);
             */
    public static int getWildernessLevel(Coordinate position) {
        int level = 0;
        if (WILDERNESS_1.contains(position)) {
            level = (position.getY() - 55 * 64) / 8 + 1;
        } else if (WILDERNESS_2.contains(position)) {
            level = (position.getY() - 155 * 64) / 8 - 1;
        } else if (WILDERNESS_3.contains(position)) {
            level = (position.getY() - 155 * 64) / 8 + 1;
        }
        return level;
    }
   
    public static short getDepth(Player player) {
        return player != null && player.getPosition() != null ? (short) getWildernessLevel(player.getPosition()) : getDepth();
    }
    
    public static short getDepth() {
        Player p = Players.getLocal();
        return p != null && p.getPosition() != null ? (short) getWildernessLevel(p.getPosition()) : 0;
    }
}
