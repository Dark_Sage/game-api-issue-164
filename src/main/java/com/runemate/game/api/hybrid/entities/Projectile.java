package com.runemate.game.api.hybrid.entities;

import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.script.annotations.*;
import javax.annotation.*;

/**
 * An entity moving over the world-graph that is targeting an Actor
 */
public interface Projectile extends Identifiable, LocatableEntity, Rotatable, Modeled, Animable {
    /**
     * An id that can be used to identify this projectile
     *
     * @deprecated use the definition's id.
     */
    @Deprecated
    default int getId() {
        SpotAnimationDefinition definition = getDefinition();
        return definition != null ? definition.getId() : -1;
    }

    /**
     * The character that this projectile originated from. This information is only available on RS3
     *
     * @deprecated Never worked as intended, purpose of hook was misunderstood.
     */
    @Deprecated
    Actor getSource();

    /**
     * The character that this projectile is targeting/attacking
     *
     * @return The character if available, otherwise null
     */
    @Nullable
    Actor getTarget();

    int getSpotAnimationId();

    @Nullable
    SpotAnimationDefinition getDefinition();

    @OSRSOnly
    int getLaunchCycle();

    int getImpactCycle();

    @OSRSOnly
    Coordinate getLaunchPosition();

    boolean hasLaunched();
}
