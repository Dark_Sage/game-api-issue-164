package com.runemate.game.api.hybrid.entities.status;

import lombok.*;

/**
 * An icon displayed over the head of npcs and players due to combat status' and ailments.
 */
public final class OverheadIcon {

    private final int index;
    private final Type type;

    public OverheadIcon(final int index) {
        this(index, Type.UNKNOWN);
    }

    public OverheadIcon(final int index, final Type type) {
        this.index = index;
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OverheadIcon that = (OverheadIcon) o;
        return index == that.index;
    }

    @Override
    public int hashCode() {
        return index;
    }

    @Override
    public String toString() {
        return "OverheadIcon(id=" + index + ", type=" + type.name() + ')';
    }

    /**
     * Gets the icons id
     */
    public int getId() {
        return index;
    }

    public Type getType() {
        return type;
    }

    public PrayerType getPrayerType() {
        if (!Type.PRAYER.equals(getType())) {
            return null;
        }
        return PrayerType.of(getId());
    }

    public SkullType getSkullType() {
        if (!Type.SKULL.equals(getType())) {
            return null;
        }
        return SkullType.of(getId());
    }

    @AllArgsConstructor
    public enum PrayerType {
        MELEE(0),
        RANGED(1),
        MAGIC(2),
        RETRIBUTION(3),
        SMITE(4),
        REDEMPTION(5),
        RANGE_MAGE(6),
        RANGE_MELEE(7),
        MAGE_MELEE(8),
        RANGE_MAGE_MELEE(9),
        WRATH(10),
        SOUL_SPLIT(11),
        DEFLECT_MELEE(12),
        DEFLECT_RANGE(13),
        DEFLECT_MAGE(14);

        private final int id;

        public static PrayerType of(int id) {
            for (final var prayer : values()) {
                if (prayer.id == id) {
                    return prayer;
                }
            }
            return null;
        }
    }

    public enum SkullType {
        BASIC,
        FIGHT_PIT,
        DEADMAN_5,
        DEADMAN_4,
        DEADMAN_3,
        DEADMAN_2,
        DEADMAN_1;

        public static SkullType of(int id) {
            switch (id) {
                case 0:
                    return BASIC;
                case 1:
                    return FIGHT_PIT;
                case 8:
                    return DEADMAN_5;
                case 9:
                    return DEADMAN_4;
                case 10:
                    return DEADMAN_3;
                case 11:
                    return DEADMAN_2;
                case 12:
                    return DEADMAN_1;
                    //unknown: 2,3,4,5,6,7
                default:
                    return null;
            }
        }
    }

    public enum Type {
        SKULL,
        PRAYER,
        UNKNOWN
    }
}
