package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.attributes.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.Menu;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.collections.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.osrs.region.*;
import com.runemate.game.api.script.annotations.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import javafx.scene.canvas.*;
import javax.annotation.*;
import lombok.*;

public abstract class InterfaceComponent
    implements Validatable, Identifiable, Interactable, Renderable, Onymous {
    public final InterfaceContainer container;
    protected final long uid;
    private final int index;
    private final InterfaceComponent parent;
    private int cachedId = -1;
    private Optional<InterfaceComponent> cachedLayer;
    //private Integer cachedLayerId;
    private Type cachedType;
    private OpenInterfaceComponent component;

    public InterfaceComponent(final InterfaceContainer container, final long uid, final int index, boolean rs3) {
        this(container, null, uid, index, rs3);
    }

    public InterfaceComponent(
        final InterfaceContainer container,
        final InterfaceComponent parent,
        final long uid,
        final int index,
        boolean rs3
    ) {
        this.container = container;
        this.parent = parent;
        this.uid = uid;
        this.index = index;
    }

    protected OpenInterfaceComponent component() {
        if (component == null) {
            component = OpenInterfaceComponent.create(uid, index);
        }
        return component;
    }


    public List<String> getActions() {
        return component().getActions();
    }

    @Deprecated
    public final int getBorderThickness() {
        return getSpriteBorderInset();
    }


    @Nullable
    public final InteractableRectangle getBounds() {
        int width = getWidth();
        if (width <= 0) {
            return null;
        }
        int height = getHeight();
        if (height <= 0) {
            return null;
        }
        InterfaceComponent layer = getLayer();
        if (layer == null) {
            return getRootBounds();
        }
        int x = 0;
        int y = 0;
        final InteractableRectangle layerBounds = layer.getBounds();
        if (layerBounds != null) {
            x = layerBounds.x;
            y = layerBounds.y;
        }
        final Point scrollShift = layer.getScrollShift();
        if (scrollShift != null) {
            x -= scrollShift.x;
            y -= scrollShift.y;
        }
        return new InteractableRectangle(
            x + component().getRelativeX(),
            y + component().getRelativeY(),
            width, height
        );
    }


    public final int getChildQuantity() {
        return component().getChildCount();
    }

    @Deprecated
    public final int getComponentQuantity() {
        return getChildQuantity();
    }

    @Deprecated
    @Nullable
    public final InterfaceComponent getComponent(int index) {
        return getChild(index);
    }

    /**
     * @deprecated use {@link #getChildren()} instead.
     */
    @Deprecated
    public final List<InterfaceComponent> getComponents() {
        return getChildren();
    }

    @Nullable
    public abstract InterfaceComponent getChild(int index);

    @Nullable
    public abstract InterfaceComponent getChild(Predicate<InterfaceComponent> predicate);

    public abstract List<InterfaceComponent> getChildren();

    public final List<InterfaceComponent> getChildren(
        final Predicate<InterfaceComponent> predicate
    ) {
        if (predicate == null) {
            return getChildren();
        }
        return newQuery().filter(predicate).results().asList();
    }

    @Deprecated
    public final List<InterfaceComponent> getComponents(
        final Predicate<InterfaceComponent> predicate
    ) {
        return getChildren(predicate);
    }

    @Nullable
    public final ItemDefinition getContainedItem() {
        int id = getContainedItemId();
        return id != -1 ? ItemDefinition.get(id) : null;
    }


    public final int getContainedItemId() {
        return component().getItemId();
    }

    public InterfaceContainer getContainer() {
        return container;
    }


    public final int getSpecializationIndicator() {
        //1337 = "peakhole"/viewport
        //1338 = minimap
        //1354 = magic spell container? 161,66
        //AKA clientcode
        return component().getContentType();
    }


    public final int getFontId() {
        return component().getFontId();
    }

    /*
    public final InterfaceComponent getDragParent() {
        long dragUID = client.rmi().uid("RTComponent.dragParent", uid);
        if (dragUID != 0) {
        //TODO make interface component object from this
        }
        return null;
    }*/


    public final int getHeight() {
        return component().getHeight();
    }


    public final int getId() {
        if (cachedId != -1) {
            return cachedId;
        }
        return cachedId = component().getId();
    }

    public final int getIndex() {
        return index;
    }

    public InterfaceComponent getLayer() {
        //Confirmed by line child.layer = (child.id = parent.id);
        if (parent != null) {
            return parent;
        }
        if (cachedLayer != null) {
            return cachedLayer.orElse(null);
        }
        int layerId = getLayerId();
        if (layerId != -1) {
            cachedLayer = Optional.ofNullable(Interfaces.getAt(layerId >>> 16, layerId & 0xFFFF));
        } else {
            cachedLayer = Optional.empty();
        }
        return cachedLayer.orElse(null);
    }


    public int getLayerId() {
        int layerId = component().getParentId();
        if (layerId != -1) {
            return layerId;
        }
        return component().getLayerId(container.getIndex());
    }

    public int getLayerDepth() {
        int depth = 0;
        InterfaceComponent layer = this;
        while ((layer = layer.getLayer()) != null) {
            depth++;
        }
        return depth;
    }

    /**
     * Gets the name of the interface as shown in the menu
     */
    @Override

    public final String getName() {
        String name = component().getName();
        return name == null || name.isEmpty() ? null : name;
    }

    @Nullable
    public InterfaceComponent getParentComponent() {
        return parent;
    }

    public final int getProjectedBufferId() {
        final int id = getProjectedEntityId();
        if (id > 0) {
            return id;
        }
        return -1;
    }

    public abstract int getContainedItemQuantity();

    /**
     * Gets the animation being used to animate the projected entity
     */
    public abstract int getProjectedEntityAnimationId();

    @RS3Only
    private List<Object> getMouseHoveringEventVariables() {
        return getMouseHoveringEventVariables(null);
    }

    @RS3Only
    protected abstract List<Object> getMouseHoveringEventVariables(Predicate<Object> condition);

    /**
     * Gets the text in the tooltip that appears after hovering the component for a moment
     *
     * @return null if the component doesn't have a tooltip, otherwise a {@link String}
     */
    @RS3Only
    @Nullable
    public String getTooltip() {
        for (Object eventVariable : getMouseHoveringEventVariables()) {
            if (eventVariable instanceof String) {
                return (String) eventVariable;
            }
        }
        return null;
    }

    /**
     * Gets the information about the entity being projected and returns it (if it's an Item)
     */
    @Nullable
    public final ItemDefinition getProjectedItem() {
        int type = getProjectedEntityType();
        if (type == 4) {
            return ItemDefinition.get(getProjectedEntityId());
        }
        return null;
    }

    /**
     * Gets the information about the entity being projected and returns it (if it's a Player)
     */
    @Nullable
    public final NpcDefinition getProjectedNpc() {
        int type = getProjectedEntityType();
        if (type == 2 || type == 6) {
            return NpcDefinition.get(getProjectedEntityId());
        }
        return null;
    }

    /**
     * Gets the information about the entity being projected and returns it (if it's a Player)
     */
    @Nullable
    public final Player getProjectedPlayer() {
        int type = getProjectedEntityType();
        if (type == 3 || type == 5) {
            final int index = getProjectedEntityId();
            if (index >= 0 && index < 2048) {
                return OSRSPlayers.getAt(index);
            } else if (index == -1) {
                return OSRSPlayers.getLocal();
            }
        } else if (type == 7) {
            return OSRSPlayers.getLocal();
        }
        return null;
    }


    @Nullable
    public Point getScrollShift() {
        Type type = getType();
        if (!Type.CONTAINER.equals(type)) {
            return null;
        }
        int xShift = component().getHorizontalScrollbarPosition();
        int yShift = component().getVerticalScrollbarPosition();
        if (xShift != 0 || yShift != 0) {
            if (Environment.isVerbose()) {
                //Found 2 cases where Container can have a shift, what else?
                Environment.getLogger().debug(
                    this + " has a type of " + getType() + " and a scroll shift of (" + xShift +
                        ", " + yShift + ")");
            }
        }
        return new Point(xShift, yShift);
    }


    public final int getSpriteBorderInset() {
        if (!Type.SPRITE.equals(getType())) {
            return 0;
        }
        return component().getBorderThickness();
    }


    @Nullable
    public final Color getSpriteFillColor() {
        if (!Type.SPRITE.equals(getType())) {
            return null;
        }
        return new Color(component().getShadowColor());
    }


    public final int getSpriteId() {
        if (!Type.SPRITE.equals(getType())) {
            return -1;
        }
        return component().getSpriteId();
    }


    public final int getSpriteRotation() {
        if (!Type.SPRITE.equals(getType())) {
            return 0;
        }
        return component().getSpriteRotation();
    }

    /**
     * Gets the text of an InterfaceComponent with type LABEL or TOOLTIP with the "jagtags" (formatting tags) stripped.
     *
     * @return #getRawText() formatted with JagTags.remove()
     */
    @Nullable
    public final String getText() {
        var text = getRawText();
        return text != null ? JagTags.remove(text) : null;
    }

    /**
     * Gets the raw text of an InterfaceComponent with type LABEL or TOOLTIP
     *
     * @return a String if the interface component is one of the specified types and it has text, otherwise null.
     */
    @Nullable
    public final String getRawText() {
        var type = getType();
        if (!Type.LABEL.equals(type) && !Type.TOOLTIP.equals(type)) {
            return null;
        }
        return component.getText();
    }

    public final Color getTextColor() {
        return new Color(component().getColor());
    }

    @Deprecated
    public final int getTextureId() {
        return getSpriteId();
    }


    public final Type getType() {
        if (cachedType == null) {
            int typeId = component().getType();
            cachedType = Type.resolve(typeId);
            if (typeId != -1 && Type.UNDOCUMENTED.equals(cachedType) && Environment.isSDK()) {
                System.err.println(
                    "[SDK] " + this + " has an undocumented type opcode of " + typeId +
                        " and should be reported with screenshots from the developer toolkit and of the game.");
            }
        }
        return cachedType;
    }


    public final int getWidth() {
        return component().getWidth();
    }

    @RS3Only

    public Attribute getAttribute(long id) {
        final Object attr = component().getAttribute(id);
        return attr == null ? null : new Attribute(id, attr);
    }

    @RS3Only

    public List<Attribute> getAttributes() {
        final List<Pair<Long, Object>> attrs = component().getAttributes();
        List<Attribute> list = new ArrayList<>();
        for (Pair<Long, Object> attr : attrs) {
            list.add(new Attribute(attr.getLeft(), attr.getRight()));
        }
        return list;
    }

    @Override
    public int hashCode() {
        int result = container.getIndex();
        result = 31 * result + (parent != null ? parent.index : 0);
        result = 31 * result + index;
        return result;
    }

    @Override
    public boolean equals(final Object o) {
        if (o instanceof InterfaceComponent) {
            InterfaceComponent casted = (InterfaceComponent) o;
            if (container.getIndex() == casted.container.getIndex()
                && index == casted.index) {
                InterfaceComponent parentA = parent;
                InterfaceComponent parentB = casted.parent;
                if (parentA == null) {
                    return parentB == null;
                } else if (parentB == null) {
                    return false;
                } else {
                    return parentA.index == parentB.index;
                }
            }
        }
        return false;
    }

    @Override
    public String toString() {
        String s = "InterfaceComponent [" + container.getIndex() + ", ";
        InterfaceComponent parent = this.parent;
        if (parent != null) {
            s += parent.index + ", ";
        }
        s += index + "]";
        return s;
    }


    @Override
    public final boolean isValid() {
        return OpenClient.validate(uid);
    }


    @Override
    public boolean isVisible() {
        //The code is accurate, the + 20 is a one cycle buffer and could could perhaps be reduce
        //It essentially gives it a 100ms buffer where it'll still return visible even if it's not
        //Due to latency in retrieving the values we can't reasonably expect them to perfectly match up as the game does.
        if (hidden()) {
            return false;
        }
        return component().getRenderCycle() + 20 >= RuneScape.getCurrentCycle();
    }

    @Override
    public double getVisibility() {
        return isVisible() ? 100 : 0;
    }

    @Override
    public boolean hasDynamicBounds() {
        return true;
    }

    @Override
    @Nullable
    public final InteractablePoint getInteractionPoint(Point origin) {
        final InteractableRectangle bounds = getBounds();
        return bounds != null ? bounds.getInteractionPoint(origin) : null;
    }

    @Override
    public final boolean contains(Point point) {
        final InteractableRectangle bounds = getBounds();
        return bounds != null && bounds.contains(point);
    }

    @Override
    public final boolean click() {
        return Mouse.click(this, Mouse.Button.LEFT);
    }

    @Override
    public final boolean interact(final Pattern action, final Pattern target) {
        return hover() && Menu.click(this, action, target);
    }

    @Override
    public final boolean interact(String action) {
        return interact(action, getName());
    }

    @Override
    public final boolean interact(Pattern action) {
        return interact(action, getName());
    }

    @Override
    public final void render(final Graphics2D g2d) {
        final Rectangle bounds = getBounds();
        if (bounds != null) {
            g2d.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
        }
    }

    @Override
    public void render(GraphicsContext gc) {
        final Rectangle bounds = getBounds();
        if (bounds != null) {
            gc.strokeRect(bounds.x, bounds.y, bounds.width, bounds.height);
        }
    }

    public InterfaceComponentQueryBuilder newQuery() {
        if (parent != null || !Type.CONTAINER.equals(getType())) {
            return new InterfaceComponentQueryBuilder().provider(Collections::emptyList);
        }
        return new InterfaceComponentQueryBuilder().provider(this::getChildren);
    }


    private int getProjectedEntityType() {
        return component().getEntityType();
    }

    /**
     * @see InterfaceComponent#getProjectedItem()
     * @see InterfaceComponent#getProjectedNpc()
     * @see InterfaceComponent#getProjectedPlayer()
     */

    private int getProjectedEntityId() {
        return component().getEntityId();
    }

    private InteractableRectangle getRootBounds() {
        int root_index = getRootBoundsIndex();
        if (root_index == -1) {
            return null;
        }
        return OSRSInterfaces.getRootBounds(root_index);
    }


    private int getRootBoundsIndex() {
        //If we have a parent the root index can still != -1
        //However, if we have a parent the layer is always equal to the parent
        //The root index is only used by us when the layer is null
        //So if the parent exists, the root index shouldn't ever be needed
        return component().getArrayIndex();
    }

    @SneakyThrows
    private boolean hidden() {
        return component().getHidden();
    }

    public enum Type {
        /**
         * Also known as a layer
         */
        CONTAINER(0),
        /**
         * Essentially an inventory
         */
        SPRITE_GRID(2),
        /**
         * Also known as a box
         */
        BOX(3),
        /**
         * Also known as "text"
         */
        LABEL(4),
        /**
         * RuneLite calls it graphic
         */
        SPRITE(5),
        MODEL(6),
        /**
         * Maybe an item list?
         */
        TEXT_INVENTORY(7),
        TOOLTIP(8),
        LINE(9),
        UNDOCUMENTED(-1);
        private final int id;

        Type(int id) {
            this.id = id;
        }

        public static Type resolve(int opcode) {
            for (Type type : values()) {
                if (type.id == opcode) {
                    return type;
                }
            }
            return UNDOCUMENTED;
        }

        public int getId() {
            return id;
        }
    }
}
