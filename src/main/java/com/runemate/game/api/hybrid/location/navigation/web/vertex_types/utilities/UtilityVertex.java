package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.utilities;

import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import java.io.*;
import java.util.*;

public abstract class UtilityVertex extends WebVertex {
    public UtilityVertex(final Coordinate position, final Collection<WebRequirement> requirements) {
        super(position, requirements);
    }

    public UtilityVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    @Override
    public Map<WebVertex, Double> getOutputCosts() {
        return Collections.emptyMap();
    }

    @Override
    public void addDirectedEdge(WebVertex target, double cost) {
        throw new UnsupportedOperationException(
            "Utility vertices can only have inputs, not outputs.");
    }

    @Override
    public boolean step(boolean prefersViewport) {
        return step();
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "UtilityVertex(" + position.getX() + ", " + position.getY() + ", " +
            position.getPlane() + ')';
    }
}
