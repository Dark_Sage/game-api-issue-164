package com.runemate.game.api.hybrid.entities;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;
import javax.annotation.*;

public interface GameObject extends LocatableEntity, Identifiable, Modeled, Animable, Rotatable {
    /**
     * Gets the type of the object(e.g. WallObject, FloorDecoration, etc)
     *
     * @return A String representing the object type
     */
    Type getType();

    /**
     * Also known as getObjectModelShape.
     * <p>0,1,2,3 = BoundaryObject
     * 4,5,6,7,8 = WallObject
     * 9,10,11,12,13,14,15,16,17,18,19,20,21 = PrimaryObject
     * 22 = FloorDecoration
     * </p>
     * 10 is the most common object model shape/type (standard)
     *
     * @return
     */
    byte getSpecializedTypeIndicator();

    /**
     * Gets the object's definition.
     *
     * @return The object's definition or null if it's unavailable.
     */
    @Nullable
    GameObjectDefinition getDefinition();

    /**
     * Gets the active 'local state' of this GameObjectDefinition if present, or the base GameObjectDefinition otherwise.
     */
    @Nullable
    default GameObjectDefinition getActiveDefinition() {
        final GameObjectDefinition base = getDefinition();
        final GameObjectDefinition local;
        return base != null && (local = base.getLocalState()) != null ? local : base;
    }

    @Nullable
    Direction getDirection();

    /**
     * @deprecated use {@link com.runemate.game.api.hybrid.entities.SpotAnimation}
     * and {@link com.runemate.game.api.hybrid.region.SpotAnimations}.
     * The implementation of this method was just a relatively accurate way of making it
     * appear as if spot animations worked the same way for GameObjects as they do Npcs.
     * That is not the case of the engine and this abstraction prevented several useful
     * effects from being detected.
     */
    @RS3Only
    @Deprecated
    Set<Integer> getSpotAnimationIds();

    enum Direction {
        NORTH(0, 0, 0),
        EAST(90, 512, 4096),
        SOUTH(180, 1024, 8192),
        WEST(270, 1536, 12288),
        NORTH_EAST(45, 0, 0),
        SOUTH_EAST(135, 512, 4096),
        SOUTH_WEST(225, 1024, 8192),
        NORTH_WEST(315, 1536, 12288);
        private final int angle;
        private final int osrsOrientation;
        private final int rs3Orientation;

        Direction(int angle, int osrsOrientation, int rs3Orientation) {
            this.angle = angle;
            this.osrsOrientation = osrsOrientation;
            this.rs3Orientation = rs3Orientation;
        }

        public int getAngle() {
            return angle;
        }

        public int getHighPrecisionOrientation(boolean rs3) {
            return rs3 ? rs3Orientation : osrsOrientation;
        }
    }

    enum Type {
        BOUNDARY,
        GROUND_DECORATION,
        PRIMARY,
        WALL_DECORATION,
        UNKNOWN;

        @Override
        public String toString() {
            final String[] words = name().split("_");
            StringBuilder finished = new StringBuilder();
            for (String word : words) {
                finished.append(word.charAt(0)).append(word.substring(1).toLowerCase());
            }
            return finished.toString();
        }
    }
}
