package com.runemate.game.api.hybrid.entities.details;

/**
 * An entity that can animate
 */
@FunctionalInterface
public interface Animable {
    /**
     * Gets the entities current animation id
     *
     * @return the current animation id, or if unavailable -1
     */
    int getAnimationId();
}
