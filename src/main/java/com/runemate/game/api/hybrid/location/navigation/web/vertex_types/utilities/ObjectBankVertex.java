package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.utilities;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class ObjectBankVertex extends BankVertex implements SerializableVertex {
    public ObjectBankVertex(
        String name, String action, Coordinate position,
        Collection<WebRequirement> requirements
    ) {
        this(Regex.getPatternForExactString(name), Regex.getPatternForExactString(action), position,
            requirements
        );
    }

    public ObjectBankVertex(
        Pattern name, Pattern action, Coordinate position,
        Collection<WebRequirement> requirements
    ) {
        super(name, action, position, requirements);
    }

    public ObjectBankVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    @Override
    public LocatableEntity getBank() {
        return GameObjects.newQuery().names(getName()).actions(getAction()).on(getPosition())
            .results().first();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getPosition()).append(getAction().pattern())
            .append(getName().pattern()).toHashCode();
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "ObjectBankVertex(name=" + getName() + ", action=" + getAction() +
            ", x=" + position.getX() + ", y=" + position.getY() + ", plane=" + position.getPlane() +
            ')';
    }

    @Override
    public int getOpcode() {
        return 4;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        Pattern pattern = getName();
        stream.writeUTF(pattern.pattern());
        stream.writeInt(pattern.flags());
        pattern = getAction();
        stream.writeUTF(pattern.pattern());
        stream.writeInt(pattern.flags());
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.name = Pattern.compile(stream.readUTF(), stream.readInt());
        this.action = Pattern.compile(stream.readUTF(), stream.readInt());
        return true;
    }
}
