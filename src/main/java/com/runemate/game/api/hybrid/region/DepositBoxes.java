package com.runemate.game.api.hybrid.region;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;

public class DepositBoxes {
    private static final GameObjectQueryBuilder DEPOSIT_BOX_QUERY =
        GameObjects.newQuery().names(
            "Deposit box", "Deposit chest",
            "Bank deposit box", "Bank Deposit Chest",
            "Pulley lift"
        ).actions("Deposit");

    public static LocatableEntityQueryResults<GameObject> getLoaded() {
        return DEPOSIT_BOX_QUERY.results();
    }

    public static DepositBoxQueryBuilder newQuery() {
        return new DepositBoxQueryBuilder();
    }
}
