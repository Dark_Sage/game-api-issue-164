package com.runemate.game.internal.input.mlp.loading;

import com.runemate.game.internal.input.mlp.activation.ActivationFunction;
import com.runemate.game.internal.input.mlp.activation.impl.Linear;
import com.runemate.game.internal.input.mlp.layers.Layer;
import org.jblas.DoubleMatrix;

import java.util.List;

public class MultilayerPerceptron {

    private final ActivationFunction activationFunction = new Linear();
    private final List<Layer> layers;


    public MultilayerPerceptron(final List<Layer> layers) {
        this.layers = layers;
    }


    /**
     * Calls the forward propagation function starting from layer 0 and uses the network inputs
     *
     * @param input network inputs
     * @return the predicted values from the network
     */
    public DoubleMatrix predict(DoubleMatrix input) {
        return this.forwardPropagation((DoubleMatrix) input.transpose(), this.layers.get(0));
    }


    /**
     * Performs the forward propagation algorithm on the provided input and the layers of the network
     * <p>
     * If the layer is null map the network activation function on the input and return
     * <p>
     * 1. Multiply the weights by the input vector
     * 2. Add the biases to the result of the above operation
     * 3. Map the activation function on the result of the above operation
     *
     * @param input      the matrix of values
     * @param firstLayer the next layer that the values will be fed into after mutation
     * @return the output of the current layer to be passed on to next layer
     */
    private DoubleMatrix forwardPropagation(DoubleMatrix input, Layer firstLayer) {

        // If we are done the provided layer is null so map the final activation function and return
        if (firstLayer == null) {
            this.map(input, this.activationFunction);
            return input;
        }

        // Multiply the weight matrix by the current value matrix
        DoubleMatrix output = firstLayer.getWeights().mmul(input);
        // Add the bias
        output.addi(firstLayer.getBiases());
        // map the layers activation function onto the layer output
        this.map(output, firstLayer.getActivationFunction());

        // recursive call
        return forwardPropagation(output, firstLayer.getOutputLayer());
    }


    /**
     * Maps the provided activation function on the provided DoubleMatrix
     * DoubleMatrix is mutated
     *
     * @param matrix             the matrix
     * @param activationFunction the activation function
     */
    private void map(final DoubleMatrix matrix, final ActivationFunction activationFunction) {

        for (int i = 0; i < matrix.rows; i++) {
            for (int j = 0; j < matrix.columns; j++) {
                matrix.put(i, j, activationFunction.activate(matrix.get(i, j)));
            }
        }

    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (Layer layer : this.layers) {
            str.append(layer.toString()).append("\n");
        }
        return str.toString();
    }
}
