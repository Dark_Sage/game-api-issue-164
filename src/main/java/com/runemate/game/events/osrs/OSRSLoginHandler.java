package com.runemate.game.events.osrs;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.osrs.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.logger.*;
import com.runemate.game.events.*;
import com.runemate.game.internal.events.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.*;
import javax.annotation.*;

/*
Some error codes:
Code 1 : Could not display video advertising logging in in...
Code 2 : Lets you login normally
Code 3 : Invalid username or password
Code 4 : Your account has been disabled
Code 5 : Your account has not logged out from its last session
Code 6 : Runescape has been updated! Please reload the page.
Code 7 : World is full
Code 8 : Unable to connect, login server offline
Code 9 : Login limit exceeded: too many connections from your address
Code 10 : Unable to connect bad session ID
Code 11 : Your password is too common
Code 12 : You need a members account to login on this world
Code 13 : Could not complete login please try a differen't world
Code 14 : The server is being updated. Please wait 1 minute and try again.
Code 15 : Unexpected Server Response
Code 16 : Too many login attempts
Code 17 : You are standing in a members only world
Code 18 : Your account has been locked
Code 19 : Fullscreen is currently a member only feature
Code 20 : Invalid login server requested
Code 21 : Stays on logging in forever?
Code 22 : Malformed login packet
Code 23 : No reply from login server
Code 24 : Error loading profile
Code 25 : Unexpected login server response
Code 26 : This computers address was used to break our rules
Code 27 : Service unavailable
Code 28 : Unexpected server response
Code 29 : Stays on logging in forever?
Code 30 : This is a members account
 */
public final class OSRSLoginHandler extends GameEventHandler {
    private int fails;

    public OSRSLoginHandler() {
        super(PRIORITY_HIGH);
    }

    public static InteractableRectangle getLoginFieldBounds() {
        Rectangle screen = Screen.getBounds();
        if (screen == null) {
            screen = new Rectangle(765, 503);
        }
        return new InteractableRectangle(0, 242, screen.width, 11);
    }

    private static boolean clearEnteredLoginName() {
        final int MAX_TRIES = Random.nextInt(3, 8);
        int tries = 0;
        int deleteKey = KeyEvent.VK_BACK_SPACE;
        while (tries < MAX_TRIES && OpenAccountDetails.getEnteredUsernameLength() > 0) {
            final int length = OpenAccountDetails.getEnteredUsernameLength();
            Keyboard.typeKey(deleteKey);
            if (length == OpenAccountDetails.getEnteredUsernameLength()) {
                if (deleteKey == KeyEvent.VK_BACK_SPACE) {
                    deleteKey = KeyEvent.VK_DELETE;
                } else {
                    deleteKey = KeyEvent.VK_BACK_SPACE;
                }
                ++tries;
            } else {
                tries = 0;
            }
        }
        return OpenAccountDetails.getEnteredUsernameLength() == 0;
    }

    private static boolean clearEnteredPassword() {
        final int MAX_TRIES = Random.nextInt(3, 8);
        int tries = 0;
        int deleteKey = KeyEvent.VK_BACK_SPACE;
        while (tries < MAX_TRIES && OpenAccountDetails.getEnteredPasswordLength() > 0) {
            final int length = OpenAccountDetails.getEnteredPasswordLength();
            Keyboard.typeKey(deleteKey);
            if (length == OpenAccountDetails.getEnteredPasswordLength()) {
                if (deleteKey == KeyEvent.VK_BACK_SPACE) {
                    deleteKey = KeyEvent.VK_DELETE;
                } else {
                    deleteKey = KeyEvent.VK_BACK_SPACE;
                }
                ++tries;
            } else {
                tries = 0;
            }
        }
        return OpenAccountDetails.getEnteredPasswordLength() == 0;
    }

    private static InteractableRectangle getExistingUserBoundsButton() {
        Rectangle screen = Screen.getBounds();
        if (screen == null) {
            screen = new Rectangle(765, 503);
        }
        return new InteractableRectangle(((screen.width - 765) / 2) + 389, 271, 144, 40);
    }

    private static InteractableRectangle getDisconnectedFromTheServerButtonBounds() {
        Rectangle screen = Screen.getBounds();
        if (screen == null) {
            screen = new Rectangle(765, 503);
        }
        return new InteractableRectangle(((screen.width - 765) / 2) + 315, 287, 138, 32);
    }

    private static void setNewWorld(boolean free, boolean members, boolean fsw) {
        WorldOverview world = null;
        if (fsw) {
            if (free && members) {
                world = Worlds.newQuery().include(WorldType.FRESH_START_WORLD).results().random();
            } else if (free) {
                world = Worlds.newQuery().include(WorldType.FRESH_START_WORLD).free().results().random();
            } else if (members) {
                world = Worlds.newQuery().include(WorldType.FRESH_START_WORLD).member().results().random();
            }
        } else {
            if (free && members) {
                world = Worlds.newQuery().regular().results().random();
            } else if (free) {
                world = Worlds.newQuery().regular().free().results().random();
            } else if (members) {
                world = Worlds.newQuery().regular().member().results().random();
            }
        }
        if (world != null) {
            if (world.getActivity().equals("King of the Skill")) {
                setNewWorld(free, members, fsw);
                return;
            }
            Environment.getLogger()
                .debug("[Login Handler] Changing the preferred world to " + world.getId());
            Worlds.setPreferred(world.getId());
        }
    }

    @Nonnull
    @Override
    public List<GameEvents.GameEvent> getChildren(AbstractBot bot) {
        return Collections.emptyList();
    }

    @Override
    public GameEvents.GameEvent getAPIEventInstance() {
        return GameEvents.Universal.LOGIN_HANDLER;
    }

    @Override
    public boolean isValid() {
        if (OpenAccountDetails.hasActiveAccount()) {
            int state = OSRSRunescape.getEngineState();
            return state == 10 || state == 11;
        }
        return false;
    }

    @Override
    public void onStart() {
        fails = 0;
    }

    @Override
    public void run() {
        BotLogger logger = Environment.getLogger();
        int preferredWorld = Worlds.getPreferred();
        if (preferredWorld != -1 && !WorldSelect.isSelected(preferredWorld) &&
            WorldSelect.isSelectable(preferredWorld)) {
            logger.fine("[Login handler] Switching worlds to " + preferredWorld);
            WorldSelect.select(preferredWorld);
        } else {
            if (OSRSWorldSelect.isOpen()) {
                OSRSWorldSelect.close();
            } else {
                final int loginState = OSRSRunescape.getLoginState();
                if (loginState == 0) {
                    logger.fine("[Login handler] Pressing Existing User");
                    if (getExistingUserBoundsButton().click()) {
                        Execution.delayUntil(() -> OSRSRunescape.getLoginState() == 2, 1000);
                    }
                } else if (loginState == 1) {
                    //High-risk pvp world warning
                    final String response =
                        OSRSRunescape.getServerResponseMessage().replace("\n", " ");
                    if (response.contains("Your normal account will not be affected")) {
                        logger.fine(
                            "[Login handler] Beta world detected, switching to a different world.");
                        Keyboard.typeKey(KeyEvent.VK_ESCAPE);
                        setNewWorld(false, true, false);
                    } else {
                        logger.fine("[Login handler] Dismissing high-risk warning");
                        Keyboard.typeKey(KeyEvent.VK_ENTER);
                        setNewWorld(false, true, false);
                    }
                } else if (loginState == 2) {
                    if (!OpenAccountDetails.isUsernameEntered()) {
                        if (OpenAccountDetails.getEnteredUsernameLength() == 0) {
                            logger.fine("[Login handler] Entering username");
                            OpenAccountDetails.typeUsername();
                            if (OpenAccountDetails.getEnteredUsernameLength() > 0) {
                                Keyboard.typeKey(KeyEvent.VK_TAB);
                            } else {
                                getLoginFieldBounds().click();
                            }
                        } else {
                            logger.fine("[Login handler] Clearing username");
                            if (!clearEnteredLoginName()) {
                                getLoginFieldBounds().click();
                            }
                        }
                    } else if (!OpenAccountDetails.isPasswordEntered()) {
                        if (OpenAccountDetails.getEnteredPasswordLength() > 0) {
                            logger.fine("[Login handler] Clearing password");
                            clearEnteredPassword();
                        } else {
                            logger.fine("[Login handler] Entering password");
                            OpenAccountDetails.typePassword();
                        }
                    } else if (Keyboard.typeKey(KeyEvent.VK_ENTER)) {
                        logger.fine("[Login handler] Logging in");
                        if (Execution.delayUntil(() -> "Connecting to server...".equals(
                            OSRSRunescape.getServerResponseMessage()), 2500)) {
                            Execution.delayWhile(() -> "Connecting to server...".equals(
                                OSRSRunescape.getServerResponseMessage())
                                || OSRSRunescape.getEngineState() == 20, 10000);
                            int state = OSRSRunescape.getEngineState();
                            if (state == 10 || state == 11) {
                                //banned/invalid credentials/game updated
                                //TODO tray notifications
                                final String response =
                                    OSRSRunescape.getServerResponseMessage().replace("\n", " ");
                                if (response.contains("Invalid username")) {
                                    GameEvents.LoginManager.getFailedLoginHandler()
                                        .handle(
                                            GameEvents.LoginManager.Fail.INVALID_CREDENTIALS,
                                            ++fails
                                        );
                                    logger.severe("[Login handler] Unable to log in: " + response);
                                    GameEventControllerImpl.displayTrayNotification(this,
                                        '"' + response + '"', TrayIcon.MessageType.NONE
                                    );
                                } else if (response.contains("Login limit exceeded")) {
                                    GameEvents.LoginManager.getFailedLoginHandler()
                                        .handle(
                                            GameEvents.LoginManager.Fail.LOGIN_LIMIT_EXCEEDED,
                                            ++fails
                                        );
                                    logger.severe("[Login handler] Unable to log in: " + response);
                                    GameEventControllerImpl.displayTrayNotification(this,
                                        '"' + response + '"', TrayIcon.MessageType.NONE
                                    );
                                } else if (response.contains("we suspect it has been stolen")) {
                                    GameEvents.LoginManager.getFailedLoginHandler()
                                        .handle(
                                            GameEvents.LoginManager.Fail.SUSPECTED_STOLEN,
                                            ++fails
                                        );
                                    logger.severe("[Login handler] Unable to log in: " + response);
                                    GameEventControllerImpl.displayTrayNotification(this,
                                        '"' + response + '"', TrayIcon.MessageType.NONE
                                    );
                                } else if (response.contains("Please reload this page.")) {
                                    GameEvents.LoginManager.getFailedLoginHandler()
                                        .handle(
                                            GameEvents.LoginManager.Fail.RUNESCAPE_UPDATED,
                                            ++fails
                                        );
                                    logger.severe("[Login handler] Unable to log in: " + response);
                                    GameEventControllerImpl.displayTrayNotification(this,
                                        '"' + response + '"', TrayIcon.MessageType.NONE
                                    );
                                } else if (response.contains(
                                    "Please choose yourself a display name via the website before logging in.")) {
                                    GameEvents.LoginManager.getFailedLoginHandler()
                                        .handle(
                                            GameEvents.LoginManager.Fail.MISSING_DISPLAY_NAME,
                                            ++fails
                                        );
                                    logger.severe("[Login handler] Unable to log in: " + response);
                                    GameEventControllerImpl.displayTrayNotification(this,
                                        '"' + response + '"', TrayIcon.MessageType.NONE
                                    );
                                }
                                else if (response.contains("digit code generated by")) {
                                    logger.severe("[Login handler] Unable to log in: " + response);
                                    OpenAccountDetails.setAccountAuthenticatorRequired();
                                    //TODO tray notifications
                                    GameEventControllerImpl.displayTrayNotification(this,
                                        '"' + response + '"', TrayIcon.MessageType.NONE
                                    );
                                    GameEvents.LoginManager.getFailedLoginHandler()
                                        .handle(
                                            GameEvents.LoginManager.Fail.AUTHENTICATOR,
                                            ++fails
                                        );
                                } else if (response.contains("need a members account")) {
                                    logger.fine(
                                        "[Login handler] Members account required, selecting non-members world");
                                    setNewWorld(true, false, false);
                                } else if (response.contains("in non-member skills")) {
                                    logger.fine(
                                        "[Login handler] Selecting a non-members world with a lower skill requirement.");
                                    setNewWorld(true, false, false);
                                } else if (response.contains("standing in a members")
                                    || response.contains("log into a members world and move")
                                    || response.contains("need a skill total")) {
                                    logger.fine(
                                        "[Login handler] In members area, selecting members world");
                                    setNewWorld(false, true, false);
                                } else if (response.contains("must be a Fresh Start Player")) {
                                    logger.fine(
                                        "[Login handler] Fresh Start account required, selecting non-fresh start world");
                                    setNewWorld(false, true, false);
                                } else if (response.contains("can't play on this world as a Fresh Start Player")) {
                                    logger.fine(
                                        "[Login handler] Fresh Start account, selecting fresh start world");
                                    setNewWorld(false, true, true);
                                } else if (response.contains("is already logged in")
                                    || response.contains("account has not logged out from its last session")
                                    || response.equals("Too many login attempts.")) {
                                    GameEventControllerImpl.displayTrayNotification(this,
                                        '"' + response + '"', TrayIcon.MessageType.NONE
                                    );
                                    GameEvents.LoginManager.getFailedLoginHandler()
                                        .handle(
                                            GameEvents.LoginManager.Fail.ALREADY_LOGGED_IN,
                                            ++fails
                                        );
                                }
                            }
                        } else {
                            final String response =
                                OSRSRunescape.getServerResponseMessage().replace("\n", " ");
                            if (response.equals("Error connecting to server.")) {
                                GameEventControllerImpl.displayTrayNotification(this,
                                    '"' + response + '"', TrayIcon.MessageType.NONE
                                );
                                GameEvents.LoginManager.getFailedLoginHandler()
                                    .handle(GameEvents.LoginManager.Fail.CONNECTION_ERROR, ++fails);
                            }
                        }
                    }
                } else if (loginState == 4) {
                    final String response =
                        OSRSRunescape.getServerResponseMessage().replace("\n", " ");
                    if (response.contains("PIN") && response.contains("enter a")) {
                        GameEventControllerImpl.displayTrayNotification(this, '"' + response + '"',
                            TrayIcon.MessageType.NONE
                        );
                        logger.severe("[Login handler] Unable to login: " + response);
                        GameEvents.LoginManager.getFailedLoginHandler()
                            .handle(GameEvents.LoginManager.Fail.AUTHENTICATOR, ++fails);
                    } else if (response.contains("need a members account")) {
                        logger.fine(
                            "[Login handler] Members account required, selecting non-member world");
                        setNewWorld(true, false, false);
                    } else if (response.contains("in non-member skills")) {
                        logger.fine(
                            "[Login handler] Selecting a non-members world with a lower skill requirement.");
                        setNewWorld(true, false, false);
                    } else if (response.contains("standing in a members")
                        || response.contains("log into a members world and move")
                        || response.contains("need a skill total")) {
                        logger.fine(
                            "[Login handler] Standing in members area, selecting members world");
                        setNewWorld(false, true, false);
                    } else if (response.contains("must be a Fresh Start Player")) {
                        logger.fine(
                            "[Login handler] Fresh Start account required, selecting non-fresh start world");
                        setNewWorld(false, true, false);
                    } else if (response.contains("can't play on this world as a Fresh Start Player")) {
                        logger.fine(
                            "[Login handler] Fresh Start account, selecting fresh start world");
                        setNewWorld(false, true, true);
                    }
                } else if (loginState == 14) {
                    final String response =
                        OSRSRunescape.getServerResponseMessage().replace("\n", " ");
                    if (response.contains("serious rule breaking")) {
                        GameEvents.LoginManager.getFailedLoginHandler()
                            .handle(GameEvents.LoginManager.Fail.BAN, ++fails);
                        logger.severe("[Login handler] Your account has been temporarily or permanently banned");
                        GameEventControllerImpl.displayTrayNotification(this,
                            "\"Your account has been temporarily or permanently banned\"", TrayIcon.MessageType.NONE
                        );
                    } else if (response.contains("suspect it has been stolen")) {
                        GameEvents.LoginManager.getFailedLoginHandler()
                            .handle(GameEvents.LoginManager.Fail.SUSPECTED_STOLEN, ++fails);
                        logger.severe("[Login handler] Your account has been disabled due to being suspected stolen");
                        GameEventControllerImpl.displayTrayNotification(this,
                            "\"Your account has been disabled due to being suspected stolen\"", TrayIcon.MessageType.NONE
                        );
                    }
                } else if (loginState == 24) {
                    GameEvents.LoginManager.getFailedLoginHandler()
                        .handle(GameEvents.LoginManager.Fail.DISCONNECTED_FROM_SERVER);
                    logger.fine(
                        "[Login handler] You were disconnected from the server. Clicking ok and logging back in.");
                    getDisconnectedFromTheServerButtonBounds().click();
                    GameEventControllerImpl.displayTrayNotification(
                        this,
                        "You were disconnected from the server. Clicking ok and logging back in.",
                        TrayIcon.MessageType.INFO
                    );
                }
            }
        }
    }
}
