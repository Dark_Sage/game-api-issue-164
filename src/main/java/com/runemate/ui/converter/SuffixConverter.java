package com.runemate.ui.converter;

import com.runemate.game.api.hybrid.*;
import com.runemate.ui.setting.annotation.open.*;
import javafx.util.*;
import lombok.*;

public class SuffixConverter extends StringConverter<Integer> {

    private final String suffix;

    public SuffixConverter(@NonNull Suffix suffix) {
        this.suffix = suffix.value();
    }

    @Override
    public String toString(final Integer object) {
        return object + suffix;
    }

    @Override
    public Integer fromString(final String string) {
        final String trimmed;
        if (string.endsWith(suffix)) {
            trimmed = string.substring(0, string.length() - suffix.length());
        } else {
            trimmed = string;
        }

        try {
            return Integer.parseInt(trimmed);
        } catch (NumberFormatException e) {
            Environment.getLogger().warn(trimmed + " is not an integer.");
            return 0;
        }
    }
}
