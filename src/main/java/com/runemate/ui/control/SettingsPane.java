package com.runemate.ui.control;

import com.runemate.ui.*;
import com.runemate.ui.setting.open.*;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import lombok.*;

public class SettingsPane extends TitledPane {

    private final SettingsManager manager;
    private final FlowPane container;
    private final DefaultUI parent;

    public SettingsPane(@NonNull DefaultUI ui, @NonNull SettingsManager manager) {
        this.manager = manager;
        this.parent = ui;

        final var parent = new VBox();
        parent.setSpacing(12);
        container = new FlowPane();
        container.setHgap(12);
        container.setVgap(12);
        container.setRowValignment(VPos.TOP);
        parent.setPadding(new Insets(12, 4, 4, 4));
        parent.getChildren().add(container);

        final var startButton = new Button("Start");
        startButton.setPrefHeight(24);
        startButton.setOnAction(e -> {
            //Start button is used exactly once, after that it can be safely hidden
            //Settings updated during bot execution should be handled by authors
            startButton.setDisable(true);
            startButton.setVisible(false);
            startButton.setManaged(false);
//            setExpanded(false);

            manager.lock();
        });
        parent.getChildren().add(startButton);

        setText("Settings");
        setContent(parent);
        addSections();

        if (!container.getChildren().isEmpty()) {
            final var first = (SettingsSectionPane) container.getChildren().get(0);
            startButton.prefWidthProperty().bind(first.widthProperty());
        } else {
            startButton.setPrefWidth(250);
        }
    }

    private void addSections() {
        final var settings = manager.getSettingsDescriptor();
        for (final var section : settings.sections()) {
            final var pane = new SettingsSectionPane(parent, section, section.section().collapsed());
            pane.init(manager, settings);
            container.getChildren().add(pane);
        }

        //Add the top-level group
        final var pane = new SettingsSectionPane(parent);
        pane.init(manager, settings);
        container.getChildren().add(0, pane);
    }
}
