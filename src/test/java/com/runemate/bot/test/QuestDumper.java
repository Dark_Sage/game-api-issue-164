package com.runemate.bot.test;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.local.*;
import java.util.*;

public class QuestDumper {

    private static final Comparator<QuestDefinition> comparator = Comparator.comparing(QuestDefinition::getType)
        .thenComparing(QuestDefinition::isMembers)
        .thenComparing(QuestDefinition::getSortName);

    public static void main(final String... arguments) {
        var defs = QuestDefinitions.loadAll();
        defs.sort(comparator);
        System.out.println(defs.size());
        for (QuestDefinition def : defs) {

            final var key = def.getDisplayName()
                .toUpperCase()
                .replace(" ", "_")
                .replaceAll("[,'.!-]", "")
                .replace("__", "_")
                .replace("&", "AND")
                .replace("RECIPE_FOR_DISASTER_", "RFD_");

            final var joiner = new StringJoiner(", ", key + "(", "),");
            joiner.add(String.valueOf(((CacheQuestDefinition) def).getRowId()));
            joiner.add(String.valueOf(def.getId()));
            joiner.add("\"" + def.getDisplayName() + "\"");

            final var progression = def.getProgressionStatus().stream().sorted().findFirst();
            progression.ifPresent(integer -> joiner.add(String.valueOf(integer)));
            joiner.add(String.valueOf(def.getCompletionStatus()));

            final var varp = Arrays.stream(VarpID.values())
                .filter(it -> it.name().contains(key))
                .findFirst()
                .map(k -> "VarpID." + k.name())
                .orElse(null);
            final var varbit = Arrays.stream(VarbitID.values())
                .filter(it -> it.name().contains(key))
                .findFirst()
                .map(k -> "VarbitID." + k.name())
                .orElse(null);

            joiner.add(Objects.requireNonNullElseGet(varp, () -> Objects.requireNonNullElse(varbit, "MISSING")));

            System.out.println(joiner);
        }
    }
}
